/**

@page page_swift_performance_optimisation Performance optimisation

<!-- Add this one for href links to subitems -->
\tableofcontents


This section discusses some Swift-specific performance optimisation steps.
While Swift 2.0 and its baseline, Peano, provide simplistic performance analysis
features to spot some flaws, a proper performance optimisation has to be guided
by external performance analysis tools.
Furthermore, please read @ref page_peano_performance_optimisation "Peano's generic performance optimisation remarks" parallel to this
page.
All of the recommendations there apply for SWIFT, too.




# Optimising the single core performance of individual species (vectorisation)

To enable proper vectorisation is not trivial within a Lagrangian framework,
as we typically need the right instructions, i.e. compute kernels, but also
a fitting memory layout.
As a user, you are responsible to ensure that the memory layout is properly
chosen, and then you can activate the solvers' vectorisation.



Some of Swift's solvers are however already well-prepared to exploit the knowledge about
optimised memory layouts and proper setups of action sets and hence can be
vectorised.
This does not hold for all solvers. You have to study their documentation,
typically alter some switches and then tune the memory layout and particle
handling as described below.
Please note that not all solvers are compatible with all graph compiler
flavours either. Again, this is something you might want to check with the
solver documentation.

Even if your solver does not support vectorisation, you might benefit from the
discussions below such as the memory pool and the different flavours of active
set handling.
The impact just won't be that big.
The page @ref swift_particle_new_solver provides further details how to write
vectorised solvers, i.e. how to prepare solvers to be able to exploit continuous,
well-administered memory.



## Step 1: Maintaining the active sets

There are two different versions how to maintain the active sets: We can either
build them up as proper sets and add particles to the set whenever we
touchCellFirstTime() and remove them from the cell when we invoke
touchCellLastTime(). Or we can work with a list, memorise how many particles we
add per touchCellFirstTime() and remove exactly this number when we backtrack
through the tree.

The set variant is robust, but it is not very fast as we have to check for each
particle in touchCellLastTime() where it is within the set and then we can
remove it. We also miss out on any adjacent memory storage. The list variant
is faster and preserves the ordering of the particles per vertex/cell (see the
remarks below), but it works if and only if the particle-mesh association
remains invariant. Such an "ordered" particle sequence allows you to use
compute kernels with coalesced memory access, i.e. vectorisation.
This insight gives rise to a third variant, where we use the coalesced memory
storage and insert sorting sweeps whenever the particle positions change, such
that we can always work with properly sorted particles.

Whether or not to use one variant or the other is a decision that's made by the
graph compiler. Some compiler flavours support different active set
construction variants, others are tied to one variant only.
By default, the project initialises its graph compiler with the variant
that does not try to vectorise or use sorted particles.
You can alter that in your code:


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
project.algorithm_steps_task_graph_compiler      = swift2.graphcompiler.map_particle_steps_onto_separate_mesh_traversals_insert_dummy_sweeps_and_vectorise
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Just pick another variant (consult namespace docu) here. Please note that these
operations do typically unfold their potential if and only if you use a fully
vectorised solver in combination with a proper particle memory management below.

Note: If you use a scheme which relies on vectorisation all the time, you have
to also alter the initialisation via

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
project.initialisation_steps_task_graph_compiler  = swift2.graphcompiler.serialise_initialisation_steps_and_map_onto_separate_mesh_traversals_for_coalescend_memory_access
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Step 2: Storage of particles and maintaining the particle-mesh association

Maintaining the association of particles to meshes is key, as Peano traverses
the mesh and issues the particle updates for those particles tied to particular
vertices or cells. We distinguish different ways how the particles themselves
are organised in the memory and how the indices over the particles are built
up and maintained. All particles are always stored on the heap, but how they
populate the heap can be changed by using different
@ref toolbox_particles_memorypool "memor pools".


You can alter the scheme per particle species by taking your
instance of peano4.toolbox.particles.ParticleSet. Each particle set hosts an
attribute generator. You can exchange the generator and thus switch
administration and storage variants per species.


Feature | Generator | Semantics
--------|--------------|----------
scattered particles, list index | peano4.toolbox.particles.ParticleSet.ParticleSetGenerator_ScatteredOnHeap_IndexByList | Scatter the particles all over the heap. The pointers per vertex to the particles are organised as a linked list.
scattered particles, vector index | peano4.toolbox.particles.ParticleSet.ParticleSetGenerator_ScatteredOnHeap_IndexByVector | Almost the same as peano4.toolbox.particles.ParticleSetGenerator_ScatteredOnHeap_IndexByList, but the lists of pointers are realised through std::vector.
arrays of particles per vertex | peano4.toolbox.particles.ParticleSet.ParticleSetGenerator_ContinuousPerVertex | The code tries to hold the particles associated with one vertex in one consecutive array. Whenever this is not possible, we use a linked list to reference the particles scattered over the memory.
global array of particles | peano4.toolbox.particles.ParticleSet.ParticleSetGenerator_GlobalContinuous | The code tries to hold the particles in one big consecutive array where all particles associated with one vertex are held en bloc.


The scattered variants holds the particles somewhere on the global heap, and
try never to move them. If particles travel within their subdomain, the code
solely update the pointers each vertex holds to the particle. As a consequence,
we might run into a fragmented heap occupation and the compute kernels will
have to deal with scattered memory access. Coalescent memory access is basically
never possible.


@image html python/peano4/toolbox/particles/ParticleSetGenerator_ScatteredOnHeap.png


In return, we don't move
(potentially heavy) particles around in memory and hence don't stress the
OS memory system. To maintain the lists of the particles per vertex, we offer
different versions. Again, the pointers can be stored within a consecutive
memory block (array), or we can use a list which adds another level of overhead
but is more flexible if particle-associations change frequently.


@image html python/peano4/toolbox/particles/ParticleSetGenerator_ContinuousPerVertex.png

We also offer a variant which @ref toolbox_particles_memorypool "stores the particles continuously within one memory chunks per vertex".
You can study toolbox::particles::memorypool::VertexWiseContinuousMemoryPool
for a discussion on its realisation.
Finally, there's also a variant which tries to hold all the particles in one
big blob, i.e. as one large array. This global sorting is realised through
toolbox::particles::memorypool::GlobalContinuousMemoryPool.


@image html python/peano4/toolbox/particles/ParticleSetGenerator_GlobalContinuous.png


The code to switch the storage scheme is relatively easy. Capture the particle
set that the Swift 2 project returns when you add a particle species:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
particle_set = project.add_particle_species(particle)
particle_set.generator = peano4.toolbox.particles.ParticleSetGenerator_ContinuousPerVertex(particle_set)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The line above changes the particle set generator, i.e. the class which maps the
particle set onto an implementation.


## Step 3: Optimise the core kernels

Once we have optimised the data layout, we can use this to start tweaking the
actual compute kernels. We can, for example, vectorise them aggresively or
offload (some of) them to a GPU. These techniques are studied in the last
sections of @ref swift_particle_new_solver.

Besides the description of "how to tune" a kernel, we strongly advise to
consult @ref swift_runtime_analysis "Swift's page on runtime analysis", too.


 */

