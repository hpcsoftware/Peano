// **********************************************************************************************
// ***                                     !!!WARNING!!!                                      ***
// *** WARNING: AUTO GENERATED FILE! DO NOT MODIFY BY HAND! YOUR CHANGES WILL BE OVERWRITTEN! ***
// ***                                     !!!WARNING!!!                                      ***
// ***                  Generated by Peano's Python API: www.peano-framework.org              ***
// **********************************************************************************************
#include "AMRRoutines.h"

#include "DGMatrices.h"
#include "Quadrature.h"

{% if USE_LIBXSMM %}
#include "Gemms.h"
{% endif %}

#include <mm_malloc.h>

{% import 'Gemms.template.h' as m with context %}{# get template macros #}

void {{FULL_QUALIFIED_NAMESPACE}}::faceUnknownsProlongation(
  {{CORRECTOR_COMPUTATION_PRECISION}}* __restrict__             lQhbndFine,
  {{CORRECTOR_COMPUTATION_PRECISION}}* __restrict__             lFhbndFine,
  const {{CORRECTOR_COMPUTATION_PRECISION}}* const __restrict__ lQhbndCoarse,
  const {{CORRECTOR_COMPUTATION_PRECISION}}* const __restrict__ lFhbndCoarse,
  const tarch::la::Vector<Dimensions - 1, int>&                 subfaceIndex
) {
{% if DIMENSIONS==3%}
  double* tmpX = static_cast<double *>(_mm_malloc(sizeof(double) * {{NUMBER_OF_DOF}} * {{NUMBER_OF_DOF_3D}} * {{NUMBER_OF_DATA_PADDED}}, {{ALIGNMENT}}));
{% endif %}

  {{CORRECTOR_COMPUTATION_PRECISION}}** projector = DGMatrices<{{CORRECTOR_COMPUTATION_PRECISION}}>::fineGridProjector1d;

    // Apply the single level prolongation operator.
{% if DIMENSIONS==2 %}
    // Will overwrite outputs, no need to set to 0.
    {{ m.gemm('face_Q_x', 'lQhbndCoarse', 'projector[subfaceIndex[0]]', 'lQhbndFine',
      '0',
      '0',
      '0') | indent(4) }}{##}
    {{ m.gemm('face_F_x', 'lFhbndCoarse', 'projector[subfaceIndex[0]]', 'lFhbndFine',
      '0',
      '0',
      '0') | indent(4) }}{##}
{% else %}{# DIMENSIONS==2#}
    for (int y = 0; y < {{NUMBER_OF_DOF}}; y++) {
      {{ m.gemm('face_Q_x', 'lQhbndCoarse', 'projector[subfaceIndex[0]]', 'tmpX',
        'y*'~NUMBER_OF_DOF*NUMBER_OF_DATA_PADDED,
        '0',
        'y*'~NUMBER_OF_DOF*NUMBER_OF_DATA_PADDED) | indent(6) }}{##}
    }
    for (int x = 0; x < {{NUMBER_OF_DOF}}; x++) {
      {{ m.gemm('face_Q_y', 'tmpX', 'projector[subfaceIndex[1]]', 'lQhbndFine',
        'x*'~NUMBER_OF_DATA_PADDED,
        '0',
        'x*'~NUMBER_OF_DATA_PADDED) | indent(6) }}{##}
    }
    for (int y = 0; y < {{NUMBER_OF_DOF}}; y++) {
      {{ m.gemm('face_F_x', 'lFhbndCoarse', 'projector[subfaceIndex[0]]', 'tmpX',
        'y*'~NUMBER_OF_DOF*NUMBER_OF_UNKNOWNS_PADDED,
        '0',
        'y*'~NUMBER_OF_DOF*NUMBER_OF_UNKNOWNS_PADDED) | indent(6) }}{##}
    }
    for (int x = 0; x < {{NUMBER_OF_DOF}}; x++) {
      {{ m.gemm('face_F_y', 'tmpX', 'projector[subfaceIndex[1]]', 'lFhbndFine',
        'x*'~NUMBER_OF_UNKNOWNS_PADDED,
        '0',
        'x*'~NUMBER_OF_UNKNOWNS_PADDED) | indent(6) }}{##}
    }
{% endif %}

{% if DIMENSIONS==3%}
  _mm_free(tmpX);
{% endif %}
}

void {{FULL_QUALIFIED_NAMESPACE}}::faceFluxRestriction(
  {{CORRECTOR_COMPUTATION_PRECISION}}* __restrict__             lFhbndCoarse,
  const {{CORRECTOR_COMPUTATION_PRECISION}}* const __restrict__ lFhbndFine,
  const tarch::la::Vector<Dimensions - 1, int>&                 subfaceIndex
) {
{% if DIMENSIONS==3%}
  double* tmpX = static_cast<double *>(_mm_malloc(sizeof(double) * {{NUMBER_OF_DOF}} * {{NUMBER_OF_DOF_3D}} * {{NUMBER_OF_DATA_PADDED}}, {{ALIGNMENT}}));
{% endif %}

  {{CORRECTOR_COMPUTATION_PRECISION}}** restrictor = DGMatrices<{{CORRECTOR_COMPUTATION_PRECISION}}>::fineGridProjector1d_T_weighted;

    // Apply the single level restriction operator.
    // Use the fine level unknowns as input in the first iteration.
{% if DIMENSIONS==2 %}
    {{ m.gemm('face_F_x_add', 'lFhbndFine', 'restrictor[subfaceIndex[0]]', 'lFhbndCoarse',
      '0',
      '0',
      '0') | indent(4) }}{##}
{% else %}{# DIMENSIONS==2#}
    for (int y = 0; y < {{NUMBER_OF_DOF}}; y++) {
      {{ m.gemm('face_F_x', 'lFhbndFine', 'restrictor[subfaceIndex[0]]', 'tmpX',
        'y*'~NUMBER_OF_DOF*NUMBER_OF_UNKNOWNS_PADDED,
        '0',
        'y*'~NUMBER_OF_DOF*NUMBER_OF_UNKNOWNS_PADDED) | indent(6) }}{##}
    }
    for (int x = 0; x < {{NUMBER_OF_DOF}}; x++) {
      {{ m.gemm('face_F_y_add', 'tmpX', 'restrictor[subfaceIndex[1]]', 'lFhbndCoarse',
        'x*'~NUMBER_OF_UNKNOWNS_PADDED,
        '0',
        'x*'~NUMBER_OF_UNKNOWNS_PADDED) | indent(6) }}{##}
    }
{% endif %}

{% if DIMENSIONS==3%}
  _mm_free(tmpX);
{% endif %}
}


void {{FULL_QUALIFIED_NAMESPACE}}::cellUnknownsProlongation(
  {{SOLUTION_STORAGE_PRECISION}}* __restrict__             luhFine,
  const {{SOLUTION_STORAGE_PRECISION}}* const __restrict__ luhCoarse,
  const tarch::la::Vector<Dimensions, int>&                subcellIndex
){

  {{SOLUTION_STORAGE_PRECISION}}** projector = DGMatrices<{{SOLUTION_STORAGE_PRECISION}}>::fineGridProjector1d;

  double* tmpX = static_cast<double *>(_mm_malloc(sizeof(double) * {{NUMBER_OF_DOF}} * {{NUMBER_OF_DOF}} * {{NUMBER_OF_DOF_3D}} * {{NUMBER_OF_DATA_PADDED}}, {{ALIGNMENT}}));
{% if DIMENSIONS==3 %}
  double* tmpY = static_cast<double *>(_mm_malloc(sizeof(double) * {{NUMBER_OF_DOF}} * {{NUMBER_OF_DOF}} * {{NUMBER_OF_DOF_3D}} * {{NUMBER_OF_DATA_PADDED}}, {{ALIGNMENT}}));
{% endif %}

  // will overwrite tmpX, no need to set to 0
  for (int zy = 0; zy < {{NUMBER_OF_DOF*NUMBER_OF_DOF_3D}}; zy++) {
    {{ m.gemm('volume_x', 'luhCoarse', 'projector[subcellIndex[0]]', 'tmpX',
      'zy*'~NUMBER_OF_DOF*NUMBER_OF_DATA,
      '0',
      'zy*'~NUMBER_OF_DOF*NUMBER_OF_DATA_PADDED) | indent(4) }}{##}
  }
  
{% if DIMENSIONS==2 %}
  for (int x = 0; x < {{NUMBER_OF_DOF}}; x++) {
    {{ m.gemm('volume_y', 'tmpX', 'projector[subcellIndex[1]]', 'luhFine',
      'x*'~NUMBER_OF_DATA_PADDED,
      '0',
      'x*'~NUMBER_OF_DATA) | indent(4) }}{##}
  }
{% else %}
  // will overwrite tmpY, no need to set to 0
  for (int z = 0; z < {{NUMBER_OF_DOF}}; z++) {
    for (int x = 0; x < {{NUMBER_OF_DOF}}; x++) {
      {{ m.gemm('volume_y', 'tmpX', 'projector[subcellIndex[1]]', 'tmpY',
        '(z*'~NUMBER_OF_DOF*NUMBER_OF_DOF~'+x)*'~NUMBER_OF_DATA_PADDED,
        '0',
        '(z*'~NUMBER_OF_DOF*NUMBER_OF_DOF~'+x)*'~NUMBER_OF_DATA_PADDED) | indent(6) }}{##}
    }
  }
  
  for (int yx = 0; yx < {{NUMBER_OF_DOF*NUMBER_OF_DOF}}; yx++) {
    {{ m.gemm('volume_z', 'tmpY', 'projector[subcellIndex[2]]', 'luhFine',
      'yx*'~NUMBER_OF_DATA_PADDED,
      '0',
      'yx*'~NUMBER_OF_DATA) | indent(4) }}{##}
  }
{% endif %}

  _mm_free(tmpX);
{% if DIMENSIONS==3 %}
  _mm_free(tmpY);
{% endif %}

}


void {{FULL_QUALIFIED_NAMESPACE}}::cellUnknownsRestriction(
  const {{SOLUTION_STORAGE_PRECISION}}* const __restrict__ luhFine,
  {{SOLUTION_STORAGE_PRECISION}}* __restrict__             luhCoarse,
  const tarch::la::Vector<Dimensions, int>&                subcellIndex
){

  double* tmpX = static_cast<double *>(_mm_malloc(sizeof(double) * {{NUMBER_OF_DOF}} * {{NUMBER_OF_DOF}} * {{NUMBER_OF_DOF_3D}} * {{NUMBER_OF_DATA_PADDED}}, {{ALIGNMENT}}));
{% if DIMENSIONS==3 %}
  double* tmpY = static_cast<double *>(_mm_malloc(sizeof(double) * {{NUMBER_OF_DOF}} * {{NUMBER_OF_DOF}} * {{NUMBER_OF_DOF_3D}} * {{NUMBER_OF_DATA_PADDED}}, {{ALIGNMENT}}));
{% endif %}

  {{SOLUTION_STORAGE_PRECISION}}** restrictor = DGMatrices<{{SOLUTION_STORAGE_PRECISION}}>::fineGridProjector1d_T_weighted;

  // will overwrite tmpX, no need to set to 0
  for (int zy = 0; zy < {{NUMBER_OF_DOF_3D*NUMBER_OF_DOF}}; zy++) {
    {{ m.gemm('volume_x', 'luhFine', 'restrictor[subcellIndex[0]]', 'tmpX',
      'zy*'~NUMBER_OF_DOF*NUMBER_OF_DATA,
      '0',
      'zy*'~NUMBER_OF_DOF*NUMBER_OF_DATA_PADDED) | indent(4) }}{##}
  }
  
{% if DIMENSIONS==2 %}
  // Add to the coarse output
  for (int x = 0; x < {{NUMBER_OF_DOF}}; x++) {
    {{ m.gemm('volume_y_add', 'tmpX', 'restrictor[subcellIndex[1]]', 'luhCoarse',
       'x*'~NUMBER_OF_DATA_PADDED,
       '0',
       'x*'~NUMBER_OF_DATA) | indent(4) }}{##}
  }
{% else %}
  // will overwrite tmpY, no need to set to 0
  for (int z = 0; z < {{NUMBER_OF_DOF}}; z++) {
    for (int x = 0; x < {{NUMBER_OF_DOF}}; x++) {
      {{ m.gemm('volume_y', 'tmpX', 'restrictor[subcellIndex[1]]', 'tmpY',
        '(z*'~NUMBER_OF_DOF*NUMBER_OF_DOF~'+x)*'~NUMBER_OF_DATA_PADDED,
        '0',
        '(z*'~NUMBER_OF_DOF*NUMBER_OF_DOF~'+x)*'~NUMBER_OF_DATA_PADDED) | indent(6) }}{##}
    }
  }
  
  // Add to the coarse output
  for (int yx = 0; yx < {{NUMBER_OF_DOF*NUMBER_OF_DOF}}; yx++) {
    {{ m.gemm('volume_z_add', 'tmpY', 'restrictor[subcellIndex[2]]', 'luhCoarse',
      'yx*'~NUMBER_OF_DATA_PADDED,
      '0',
      'yx*'~NUMBER_OF_DATA) | indent(4) }}{##}
  }
{% endif %}

  _mm_free(tmpX);
{% if DIMENSIONS==3%}
  _mm_free(tmpY);
{% endif %}

}