// **********************************************************************************************
// ***                                     !!!WARNING!!!                                      ***
// *** WARNING: AUTO GENERATED FILE! DO NOT MODIFY BY HAND! YOUR CHANGES WILL BE OVERWRITTEN! ***
// ***                                     !!!WARNING!!!                                      ***
// ***                  Generated by Peano's Python API: www.peano-framework.org              ***
// **********************************************************************************************
#include "FusedSpaceTimePredictorVolumeIntegral.h"

#include "DGMatrices.h"
#include "Quadrature.h"
#include "CellData.h"

{% if USE_LIBXSMM %}
#include "Gemms.h"
{% endif %}

#include <cstring>
#include <algorithm>

{% import 'Gemms.template.h' as m with context %}{# get template macros #}

{% for item in NAMESPACE -%}
namespace {{ item }} {
{%- endfor %}

template <typename cStoreType, typename pCompType>
pCompType picardIteration(
  {{FULL_QUALIFIED_SOLVER_NAME}}&               solver,
  pCompType* __restrict__                       lQi,
  pCompType* __restrict__                       rhs,
  pCompType* __restrict__                       lFi,
  pCompType* __restrict__                       lSi,
  pCompType* __restrict__                       gradQ,
  pCompType* __restrict__                       new_lQi_slice,
  const pCompType* __restrict__                 dudxT_by_dx,
  const cStoreType* const __restrict__          luh,
  const tarch::la::Vector<Dimensions, double>&  cellCentre,
  const tarch::la::Vector<Dimensions, double>&  cellSize,
  const double                                  timeStamp,
  const double                                  timeStepSize
);

{% for item in NAMESPACE -%}
}
{%- endfor %}

template <typename cStoreType, typename pCompType, typename pStoreType>
int {{FULL_QUALIFIED_NAMESPACE}}::fusedSpaceTimePredictorVolumeIntegral(
  {{FULL_QUALIFIED_SOLVER_NAME}}&               solver,
  pStoreType* __restrict__                      lQhbnd,
  pStoreType* __restrict__                      lFhbnd,
  cStoreType* const __restrict__                luh,
  const tarch::la::Vector<Dimensions, double>&  cellCentre,
  const tarch::la::Vector<Dimensions, double>&  cellSize,
  const double                                  timeStamp,
  const double                                  timeStepSize
) {
  pCompType memory[getFusedSTPVISize()] __attribute__((aligned({{ALIGNMENT}})));

  pCompType* lQi   = memory + getlQiShift();
  pCompType* lQhi  = memory + getlQhiShift();

  pCompType* rhs   = memory + getRhsShift();

  {% if USE_FLUX %}
  pCompType* lFi   = memory + getlFiShift();
  pCompType* lFhi  = memory + getlFhiShift();
  {% else %}
  pCompType* lFi   = nullptr;
  {% endif %}

  {% if USE_SOURCE or USE_NCP %}
  pCompType* lSi   = memory + getlSiShift();
  pCompType* lShi  = memory + getlShiShift();
  {% else %}
  pCompType* lSi   = nullptr;
  {% endif %}

  {% if USE_NCP %}
  pCompType* gradQ = memory + getGradQShift();
  {% else %}
  pCompType* gradQ = nullptr;
  {% endif %}

  // Assume dx[0] == dx[1] == dx[2]
  assertionNumericalEquals(cellSize[0], cellSize[1]);
  #if Dimensions == 3
  assertionNumericalEquals(cellSize[0], cellSize[2]);
  #endif

  const double invDx = 1.0 / cellSize[0];

  const pCompType* Kxi    = DGMatrices<pCompType>::Kxi;
  const pCompType* Kxi_T  = DGMatrices<pCompType>::Kxi_T;
  const pCompType* iK1_T  = DGMatrices<pCompType>::iK1_T;

  //********************
  //****** Picard ******
  //********************

  pCompType new_lQi_slice[{{NUMBER_OF_DOF*NUMBER_OF_UNKNOWNS_PADDED}}] __attribute__((aligned({{ALIGNMENT}})));
  const pCompType dtByDx = invDx * timeStepSize;

{% if USE_NCP %}
  pCompType dudxT_by_dx[{{NUMBER_OF_DOF*NUMBER_OF_DOF_PADDED}}] __attribute__((aligned({{ALIGNMENT}})));
  // 0: Precompute 1/dx * dudx_T.
  for (int it = 0; it < {{NUMBER_OF_DOF*NUMBER_OF_DOF_PADDED}}; it++) {
    dudxT_by_dx[it] = invDx * DGMatrices<pCompType>::dudx_T[it];
  }
{% else %}
    pCompType* dudxT_by_dx = nullptr;
{% endif %}

  // 1: Trivial initial guess
  for (int t = 0; t < {{NUMBER_OF_DOF}}; t++) {
    for (int xyz = 0; xyz < {{NUMBER_OF_DOF**DIMENSIONS}}; xyz++) {
      std::copy_n(&luh[{{NUMBER_OF_DATA}} * xyz], {{NUMBER_OF_DATA}}, &lQi[{{NUMBER_OF_DATA_PADDED}} * (xyz + {{NUMBER_OF_DOF**DIMENSIONS}} * t)]);
    }
  }

  // 2: Discrete Picard iterations
  int numberOfPicardIterations = 0;
  for (; numberOfPicardIterations < {{2*NUMBER_OF_DOF+1}}; numberOfPicardIterations++) {
    pCompType sqRes = picardIteration<cStoreType, pCompType>(
      solver,
      lQi,
      rhs,
      lFi,
      lSi,
      gradQ,
      new_lQi_slice,
      dudxT_by_dx,
      luh,
      cellCentre,
      cellSize,
      timeStamp,
      timeStepSize
    );

    if (sqRes < (1e-7 * 1e-7)) {
      break;
    }
  }

  //***********************
  //****** Predictor ******
  //***********************

  std::memset(lQhi, 0, {{(NUMBER_OF_DOF**DIMENSIONS)*NUMBER_OF_DATA_PADDED}} * sizeof(pCompType));

{% if USE_FLUX %}
  std::memset(lFhi, 0, {{DIMENSIONS*(NUMBER_OF_DOF**DIMENSIONS)*NUMBER_OF_UNKNOWNS_PADDED}} * sizeof(pCompType));
{% endif %}

{% if USE_NCP or USE_SOURCE %}
  std::memset(lShi, 0, {{(NUMBER_OF_DOF**DIMENSIONS)*NUMBER_OF_UNKNOWNS_PADDED}} * sizeof(pCompType));
{% endif %}

  for (int z = 0; z < {{NUMBER_OF_DOF_3D}}; z++) {
    for (int y = 0; y < {{NUMBER_OF_DOF}}; y++) {
      for (int x = 0; x < {{NUMBER_OF_DOF}}; x++) {
        // Matrix-Vector Products
        for (int t = 0; t < {{NUMBER_OF_DOF}}; t++) {
          #ifdef SharedOMP
          #pragma omp simd aligned(lQhi,lQi:{{ALIGNMENT}})
          #endif
          for (int n = 0; n < {{NUMBER_OF_DATA_PADDED}}; n++) {
            lQhi[((z * {{NUMBER_OF_DOF}} + y) * {{NUMBER_OF_DOF}} + x) * {{NUMBER_OF_DATA_PADDED}} + n] += Quadrature<pCompType>::weights1[t]
              * lQi[(((t * {{NUMBER_OF_DOF_3D}} + z) * {{NUMBER_OF_DOF}} + y) * {{NUMBER_OF_DOF}} + x) * {{NUMBER_OF_DATA_PADDED}} + n];
          }
{% if USE_FLUX %}
          #ifdef SharedOMP
          #pragma omp simd aligned(lFhi,lFi:{{ALIGNMENT}})
          #endif
          for (int n = 0; n<{{NUMBER_OF_UNKNOWNS_PADDED}}; n++) {
            lFhi[((z * {{NUMBER_OF_DOF}} + y) * {{NUMBER_OF_DOF}} + x) * {{NUMBER_OF_UNKNOWNS_PADDED}} + n + {{0*NUMBER_OF_UNKNOWNS_PADDED*(NUMBER_OF_DOF**DIMENSIONS)}}] += Quadrature<pCompType>::weights1[t]
              * lFi[(((t * {{NUMBER_OF_DOF_3D}} + z) * {{NUMBER_OF_DOF}} + y) * {{NUMBER_OF_DOF}} + x) * {{NUMBER_OF_UNKNOWNS_PADDED}} + n + {{0*(NUMBER_OF_DOF**DIMENSIONS)*NUMBER_OF_DOF*NUMBER_OF_UNKNOWNS_PADDED}}];
          }
          #ifdef SharedOMP
          #pragma omp simd aligned(lFhi,lFi:{{ALIGNMENT}})
          #endif
          for (int n = 0; n<{{NUMBER_OF_UNKNOWNS_PADDED}}; n++) {
            lFhi[((z * {{NUMBER_OF_DOF}} + x) * {{NUMBER_OF_DOF}} + y) * {{NUMBER_OF_UNKNOWNS_PADDED}} + n + {{1*NUMBER_OF_UNKNOWNS_PADDED*(NUMBER_OF_DOF**DIMENSIONS)}}] += Quadrature<pCompType>::weights1[t]
              * lFi[(((t * {{NUMBER_OF_DOF_3D}} + z) * {{NUMBER_OF_DOF}} + y) * {{NUMBER_OF_DOF}} + x) * {{NUMBER_OF_UNKNOWNS_PADDED}} + n + {{1*(NUMBER_OF_DOF**DIMENSIONS)*NUMBER_OF_DOF*NUMBER_OF_UNKNOWNS_PADDED}}];
          }
{% if DIMENSIONS==3%}
          #ifdef SharedOMP
          #pragma omp simd aligned(lFhi,lFi:{{ALIGNMENT}})
          #endif
          for (int n = 0; n < {{NUMBER_OF_UNKNOWNS_PADDED}}; n++) {
            lFhi[((y * {{NUMBER_OF_DOF}} + x) * {{NUMBER_OF_DOF}} + z) * {{NUMBER_OF_UNKNOWNS_PADDED}} + n + {{2*NUMBER_OF_UNKNOWNS_PADDED*(NUMBER_OF_DOF**DIMENSIONS)}}] += Quadrature<pCompType>::weights1[t]
              * lFi[(((t * {{NUMBER_OF_DOF_3D}} + z) * {{NUMBER_OF_DOF}} + y) * {{NUMBER_OF_DOF}} + x) * {{NUMBER_OF_UNKNOWNS_PADDED}} + n + {{2*(NUMBER_OF_DOF**DIMENSIONS)*NUMBER_OF_DOF*NUMBER_OF_UNKNOWNS_PADDED}}];
          }
{% endif %}
{% endif %}{# USE_FLUX #}
{% if USE_NCP or USE_SOURCE %}
          #ifdef SharedOMP
          #pragma omp simd aligned(lShi,lSi:{{ALIGNMENT}})
          #endif
          for (int n = 0; n < {{NUMBER_OF_UNKNOWNS_PADDED}}; n++) {
            lShi[((z * {{NUMBER_OF_DOF}} + y) * {{NUMBER_OF_DOF}} + x) * {{NUMBER_OF_UNKNOWNS_PADDED}} + n] += Quadrature<pCompType>::weights1[t]
              * lSi[(((t * {{NUMBER_OF_DOF_3D}} + z) * {{NUMBER_OF_DOF}} + y) * {{NUMBER_OF_DOF}} + x) * {{NUMBER_OF_UNKNOWNS_PADDED}} + n];
          }
{% endif %}
        }
      }
    }
  }

  //**************************
  //****** Extrapolator ******
  //**************************

  std::memset(lQhbnd, 0, {{2*DIMENSIONS*NUMBER_OF_DATA_PADDED*NUMBER_OF_DOF*NUMBER_OF_DOF_3D}} * sizeof(pStoreType));
  std::memset(lFhbnd, 0, {{2*DIMENSIONS*NUMBER_OF_UNKNOWNS_PADDED*NUMBER_OF_DOF*NUMBER_OF_DOF_3D}} * sizeof(pStoreType));

  // x-direction: face 1 (left) and face 2 (right)
  for (int yz = 0; yz < {{NUMBER_OF_DOF*NUMBER_OF_DOF_3D}}; yz++) {
    // Matrix-Vector Products
    for (int x = 0; x < {{NUMBER_OF_DOF}}; x++) {
      #ifdef SharedOMP
      #pragma omp simd aligned(lQhbnd,lQhi:{{ALIGNMENT}})
      #endif
      for (int n = 0; n < {{NUMBER_OF_DATA_PADDED}}; n++) {
        lQhbnd[n + {{NUMBER_OF_DATA_PADDED}} * yz + {{0*NUMBER_OF_DATA_PADDED*NUMBER_OF_DOF*NUMBER_OF_DOF_3D}}]
          += lQhi[n + {{NUMBER_OF_DATA_PADDED}} * (x + {{NUMBER_OF_DOF}} * yz)] * DGMatrices<pCompType>::FLCoeff[x];

        lQhbnd[n + {{NUMBER_OF_DATA_PADDED}} * yz + {{1*NUMBER_OF_DATA_PADDED*NUMBER_OF_DOF*NUMBER_OF_DOF_3D}}]
          += lQhi[n + {{NUMBER_OF_DATA_PADDED}} * (x + {{NUMBER_OF_DOF}} * yz)] * DGMatrices<pCompType>::FRCoeff[x];
{% if USE_FLUX %}
{% if NUMBER_OF_DATA_PADDED != NUMBER_OF_UNKNOWNS_PADDED %}
      }
      #ifdef SharedOMP
      #pragma omp simd aligned(lFhbnd,lFhi:{{ALIGNMENT}})
      #endif
      for (int n = 0; n < {{NUMBER_OF_UNKNOWNS_PADDED}}; n++) {
{% endif %}
        lFhbnd[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * yz + {{0*NUMBER_OF_UNKNOWNS_PADDED*NUMBER_OF_DOF*NUMBER_OF_DOF_3D}}]
          += lFhi[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * (x + {{NUMBER_OF_DOF}} * yz)] * DGMatrices<pCompType>::FLCoeff[x];

        lFhbnd[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * yz + {{1*NUMBER_OF_UNKNOWNS_PADDED*NUMBER_OF_DOF*NUMBER_OF_DOF_3D}}]
          += lFhi[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * (x + {{NUMBER_OF_DOF}} * yz)] * DGMatrices<pCompType>::FRCoeff[x];
{% endif %}{# USE_FLUX #}
      }
    }
  }

  // y-direction: face 3 (left) and face 4 (right)
  for (int xz = 0; xz < {{NUMBER_OF_DOF*NUMBER_OF_DOF_3D}}; xz++) {
    // Matrix-Vector Products
    for (int y = 0; y < {{NUMBER_OF_DOF}}; y++) {
{% if DIMENSIONS==3 %}
      const int z = xz / {{NUMBER_OF_DOF}};
      const int x = xz % {{NUMBER_OF_DOF}};
{% else %}
      const int z = 0;
      const int x = xz;
{% endif %}
      #ifdef SharedOMP
      #pragma omp simd aligned(lQhbnd,lQhi:{{ALIGNMENT}})
      #endif
      for (int n = 0; n < {{NUMBER_OF_DATA_PADDED}}; n++) {
        lQhbnd[n + {{NUMBER_OF_DATA_PADDED}} * xz + {{2*NUMBER_OF_DATA_PADDED*NUMBER_OF_DOF*NUMBER_OF_DOF_3D}}]
          += lQhi[n + {{NUMBER_OF_DATA_PADDED}} * (x + {{NUMBER_OF_DOF}} * (y + {{NUMBER_OF_DOF_3D}} * z))] * DGMatrices<pCompType>::FLCoeff[y];

        lQhbnd[n + {{NUMBER_OF_DATA_PADDED}} * xz + {{3*NUMBER_OF_DATA_PADDED*NUMBER_OF_DOF*NUMBER_OF_DOF_3D}}]
          += lQhi[n + {{NUMBER_OF_DATA_PADDED}} * (x + {{NUMBER_OF_DOF}} * (y + {{NUMBER_OF_DOF_3D}} * z))] * DGMatrices<pCompType>::FRCoeff[y];
{% if USE_FLUX %}
{% if NUMBER_OF_DATA_PADDED != NUMBER_OF_UNKNOWNS_PADDED %}
      }
      #ifdef SharedOMP
      #pragma omp simd aligned(lFhbnd,lFhi:{{ALIGNMENT}})
      #endif
      for (int n = 0; n < {{NUMBER_OF_UNKNOWNS_PADDED}}; n++) {
{% endif %}
        lFhbnd[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * xz + {{2*NUMBER_OF_UNKNOWNS_PADDED*NUMBER_OF_DOF*NUMBER_OF_DOF_3D}}]
          += lFhi[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * (y + {{NUMBER_OF_DOF}} * xz) + {{1*NUMBER_OF_UNKNOWNS_PADDED*(NUMBER_OF_DOF**DIMENSIONS)}}] * DGMatrices<pCompType>::FLCoeff[y];

        lFhbnd[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * xz + {{3*NUMBER_OF_UNKNOWNS_PADDED*NUMBER_OF_DOF*NUMBER_OF_DOF_3D}}]
          += lFhi[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * (y + {{NUMBER_OF_DOF}} * xz) + {{1*NUMBER_OF_UNKNOWNS_PADDED*(NUMBER_OF_DOF**DIMENSIONS)}}] * DGMatrices<pCompType>::FRCoeff[y];
{% endif %}{# USE_FLUX #}
      }
    }
  }

{% if DIMENSIONS==3 %}
  // z-direction: face 5 (left) and face 6 (right)
  for (int xy = 0; xy < {{NUMBER_OF_DOF*NUMBER_OF_DOF}}; xy++) {
    // Matrix-Vector Products
    for (int z = 0; z < {{NUMBER_OF_DOF}}; z++) {
      #ifdef SharedOMP
      #pragma omp simd aligned(lQhbnd,lQhi:{{ALIGNMENT}})
      #endif
      for (int n = 0; n < {{NUMBER_OF_DATA_PADDED}}; n++) {
        lQhbnd[n + {{NUMBER_OF_DATA_PADDED}} * xy + {{4*NUMBER_OF_DATA_PADDED*NUMBER_OF_DOF*NUMBER_OF_DOF_3D}}]
          += lQhi[n + {{NUMBER_OF_DATA_PADDED}} * (xy + {{NUMBER_OF_DOF*NUMBER_OF_DOF}} * z)] * DGMatrices<pCompType>::FLCoeff[z];

        lQhbnd[n + {{NUMBER_OF_DATA_PADDED}} * xy + {{5*NUMBER_OF_DATA_PADDED*NUMBER_OF_DOF*NUMBER_OF_DOF_3D}}]
          += lQhi[n + {{NUMBER_OF_DATA_PADDED}} * (xy + {{NUMBER_OF_DOF*NUMBER_OF_DOF}} * z)] * DGMatrices<pCompType>::FRCoeff[z];
{% if USE_FLUX %}
{% if NUMBER_OF_DATA_PADDED != NUMBER_OF_UNKNOWNS_PADDED %}
      }
      #ifdef SharedOMP
      #pragma omp simd aligned(lFhbnd,lFhi:{{ALIGNMENT}})
      #endif
      for (int n = 0; n < {{NUMBER_OF_UNKNOWNS_PADDED}}; n++) {
{% endif %}
        lFhbnd[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * xy + {{4*NUMBER_OF_UNKNOWNS_PADDED*NUMBER_OF_DOF*NUMBER_OF_DOF_3D}}]
          += lFhi[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * (z + {{NUMBER_OF_DOF}} * xy) + {{2*NUMBER_OF_UNKNOWNS_PADDED*(NUMBER_OF_DOF**DIMENSIONS)}}] * DGMatrices<pCompType>::FLCoeff[z];

        lFhbnd[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * xy + {{5*NUMBER_OF_UNKNOWNS_PADDED*NUMBER_OF_DOF*NUMBER_OF_DOF_3D}}]
          += lFhi[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * (z + {{NUMBER_OF_DOF}} * xy) + {{2*NUMBER_OF_UNKNOWNS_PADDED*(NUMBER_OF_DOF**DIMENSIONS)}}] * DGMatrices<pCompType>::FRCoeff[z];
{% endif %}{# USE_FLUX #}
      }
    }
  }
{% endif %}{# DIMENSIONS==3 #}

  //*****************************
  //****** Volume Integral ******
  //*****************************

  // Define coefficient matrix coeffVolume
  pCompType coeffVolume[{{NUMBER_OF_DOF_PADDED*NUMBER_OF_DOF}}] __attribute__((aligned({{ALIGNMENT}})));
  for(int i=0; i<{{NUMBER_OF_DOF}}; i++) {
    for(int j=0; j<{{NUMBER_OF_DOF}}; j++) {
      coeffVolume[i * {{NUMBER_OF_DOF_PADDED}} + j] = Kxi_T[i * {{NUMBER_OF_DOF_PADDED}} + j] *
        invDx * timeStepSize * Quadrature<pCompType>::iweights1[i];
    }
  }

{% if USE_FLUX %}
  // Assume dx[0] == dx[1] == dx[2]
  for (int j = 0; j < {{NUMBER_OF_DOF_3D}}; j++) {
    for (int i = 0; i < {{NUMBER_OF_DOF}}; i++) {
      // x
      {{ m.gemm('lduh_x', 'lFhi', 'coeffVolume', 'luh',
        '(j*'~NUMBER_OF_DOF~'+i)*'~(NUMBER_OF_UNKNOWNS_PADDED*NUMBER_OF_DOF)~'+'~(0*NUMBER_OF_UNKNOWNS_PADDED*(NUMBER_OF_DOF**DIMENSIONS)),
        '0',
        '(j*'~NUMBER_OF_DOF~'+i)*'~(NUMBER_OF_DATA*NUMBER_OF_DOF) ) | indent(6) }}{##}

      // y
      {{ m.gemm('lduh_y', 'lFhi', 'coeffVolume', 'luh',
        '(j*'~NUMBER_OF_DOF~'+i)*'~(NUMBER_OF_UNKNOWNS_PADDED*NUMBER_OF_DOF)~'+'~(1*NUMBER_OF_UNKNOWNS_PADDED*(NUMBER_OF_DOF**DIMENSIONS)),
        '0',
        '(j*'~(NUMBER_OF_DOF*NUMBER_OF_DOF)~'+i)*'~NUMBER_OF_DATA) | indent(6) }}{##}

{% if DIMENSIONS==3 %}
      // z
      {{ m.gemm('lduh_z', 'lFhi', 'coeffVolume', 'luh',
        '(j*'~NUMBER_OF_DOF~'+i)*'~(NUMBER_OF_UNKNOWNS_PADDED*NUMBER_OF_DOF)~'+'~(2*NUMBER_OF_UNKNOWNS_PADDED*(NUMBER_OF_DOF**DIMENSIONS)),
        '0',
        '(j*'~NUMBER_OF_DOF~'+i)*'~NUMBER_OF_DATA) | indent(6) }}{##}
{% endif %}
    }
  }
{% endif %}{# USE_FLUX #}

{% if USE_NCP or USE_SOURCE %}
  for (int xyz = 0; xyz < {{NUMBER_OF_DOF**DIMENSIONS}}; xyz++) {
    #ifdef SharedOMP
    #pragma omp simd aligned(luh,lShi:{{ALIGNMENT}})
    #endif
    for (int n = 0; n < {{NUMBER_OF_UNKNOWNS_PADDED}}; n++) {
      luh[xyz * {{NUMBER_OF_DATA}} + n] += timeStepSize * lShi[xyz * {{NUMBER_OF_UNKNOWNS_PADDED}} + n];
    }
  }
{% endif %}

  // Return number of Picard iterations, std::min to avoid doing a +1 if the loop has not been exited early.
  return std::min(numberOfPicardIterations + 1, {{2*NUMBER_OF_DOF+1}});
}

{% for PRECISION_NUM in range(0,PREDICTOR_COMPUTATION_PRECISIONS|length) %}
template int {{FULL_QUALIFIED_NAMESPACE}}::fusedSpaceTimePredictorVolumeIntegral<{{SOLUTION_STORAGE_PRECISION}}, {{PREDICTOR_COMPUTATION_PRECISIONS[PRECISION_NUM]}}, {{CORRECTOR_COMPUTATION_PRECISION}}>(
  {{FULL_QUALIFIED_SOLVER_NAME}}&                                   solver,
  {{CORRECTOR_COMPUTATION_PRECISION}}* __restrict__                 lQhbnd,
  {{CORRECTOR_COMPUTATION_PRECISION}}* __restrict__                 lFhbnd,
  {{SOLUTION_STORAGE_PRECISION}}* const __restrict__                luh,
  const tarch::la::Vector<Dimensions, double>&                      cellCentre,
  const tarch::la::Vector<Dimensions, double>&                      cellSize,
  const double                                                      timeStamp,
  const double                                                      timeStepSize
);
{% endfor %}

template <typename cStoreType, typename pCompType>
pCompType {{FULL_QUALIFIED_NAMESPACE}}::picardIteration(
  {{FULL_QUALIFIED_SOLVER_NAME}}&               solver,
  pCompType* __restrict__                       lQi,
  pCompType* __restrict__                       rhs,
  pCompType* __restrict__                       lFi,
  pCompType* __restrict__                       lSi,
  pCompType* __restrict__                       gradQ,
  pCompType* __restrict__                       new_lQi_slice,
  const pCompType* __restrict__                 dudxT_by_dx,
  const cStoreType* const __restrict__          luh,
  const tarch::la::Vector<Dimensions, double>&  cellCentre,
  const tarch::la::Vector<Dimensions, double>&  cellSize,
  const double                                  timeStamp,
  const double                                  timeStepSize
) {
  // Assume dx[0] == dx[1] == dx[2]
  const double invDx = 1.0 / cellSize[0];

  const pCompType* Kxi    = DGMatrices<pCompType>::Kxi;
  const pCompType* iK1_T  = DGMatrices<pCompType>::iK1_T;
  const pCompType dtByDx = invDx * timeStepSize;

  for (int t = 0; t < {{NUMBER_OF_DOF}}; t++) {  // Time DOFs
{% if USE_NCP %}
    std::memset(gradQ, 0, {{(NUMBER_OF_DOF**DIMENSIONS)*NUMBER_OF_UNKNOWNS_PADDED*DIMENSIONS}} * sizeof(pCompType));

    // Compute the "derivatives" (contributions of the stiffness matrix)
    // x-direction (independent from the y- and z-derivatives)
    for (int z = 0; z < {{NUMBER_OF_DOF_3D}}; z++) {
      for (int y = 0; y < {{NUMBER_OF_DOF}}; y++) {
        {{ m.gemm('gradQ_x', 'lQi', 'dudxT_by_dx', 'gradQ',
          '((t*'~NUMBER_OF_DOF_3D~'+z)*'~NUMBER_OF_DOF~'+y)*'~NUMBER_OF_DOF*NUMBER_OF_DATA_PADDED,
          '0',
          '(z*'~NUMBER_OF_DOF~'+y)*'~NUMBER_OF_UNKNOWNS_PADDED*NUMBER_OF_DOF) | indent(10) }}{##}
      }
    }

    // y-direction (independent from the x- and z-derivatives)
    for (int z = 0; z < {{NUMBER_OF_DOF_3D}}; z++) {
      for (int x = 0; x < {{NUMBER_OF_DOF}}; x++) {
        {{ m.gemm('gradQ_y', 'lQi', 'dudxT_by_dx', 'gradQ',
          '((t*'~NUMBER_OF_DOF_3D~'+z)*'~NUMBER_OF_DOF*NUMBER_OF_DOF~'+x)*'~NUMBER_OF_DATA_PADDED,
          '0',
          '(z*'~NUMBER_OF_DOF*NUMBER_OF_DOF~'+x)*'~NUMBER_OF_UNKNOWNS_PADDED~'+'~NUMBER_OF_UNKNOWNS_PADDED*(NUMBER_OF_DOF**DIMENSIONS)) | indent(10) }}{##}
      }
    }

{% if DIMENSIONS==3 %}
    // z-direction (independent from the x- and y-derivatives)
    for (int y = 0; y < {{NUMBER_OF_DOF}}; y++) {
      for (int x = 0; x < {{NUMBER_OF_DOF}}; x++) {
        {{ m.gemm('gradQ_z', 'lQi', 'dudxT_by_dx', 'gradQ',
          '((t*'~NUMBER_OF_DOF_3D*NUMBER_OF_DOF~'+y)*'~NUMBER_OF_DOF~'+x)*'~NUMBER_OF_DATA_PADDED,
          '0',
          '(y*'~NUMBER_OF_DOF~'+x)*'~NUMBER_OF_UNKNOWNS_PADDED~'+'~2*NUMBER_OF_UNKNOWNS_PADDED*(NUMBER_OF_DOF**DIMENSIONS)) | indent(10) }}{##}
      }
    }
{% endif %}
{% endif %}{# USE_NCP #}

{% if USE_FLUX %}
    pCompType* F[{{DIMENSIONS}}];

    for (int xyz = 0; xyz < {{NUMBER_OF_DOF**DIMENSIONS}}; xyz++) {
      F[0] = &lFi[(t * {{NUMBER_OF_DOF**DIMENSIONS}} + xyz) * {{NUMBER_OF_UNKNOWNS_PADDED}}];
      F[1] = &lFi[(t * {{NUMBER_OF_DOF**DIMENSIONS}} + xyz) * {{NUMBER_OF_UNKNOWNS_PADDED}} + {{1*(NUMBER_OF_DOF**DIMENSIONS)*NUMBER_OF_DOF*NUMBER_OF_UNKNOWNS_PADDED}}];
{% if DIMENSIONS==3 %}
      F[2] = &lFi[(t * {{NUMBER_OF_DOF**DIMENSIONS}} + xyz) * {{NUMBER_OF_UNKNOWNS_PADDED}} + {{2*(NUMBER_OF_DOF**DIMENSIONS)*NUMBER_OF_DOF*NUMBER_OF_UNKNOWNS_PADDED}}];
{% endif %}

      for(int d = 0; d < Dimensions; d++){
        solver.flux(
          lQi + (t * {{NUMBER_OF_DOF**DIMENSIONS}} + xyz) * {{NUMBER_OF_DATA_PADDED}},
          cellCentre, // TODO: Fix to node position
          cellSize,
          timeStamp, //+ Quadrature<pCompType>::nodes[t] * timeStepSize // Exact timeStamp for node
          timeStepSize,
          d,
          F[d]
        );
      }
    }
{% endif %}{# USE_FLUX #}

    // Compute the contribution of the initial condition uh to the right-hand side (rhs)
    for (int xyz = 0; xyz < {{NUMBER_OF_DOF**DIMENSIONS}}; xyz++) {
      const pCompType weight = Quadrature<pCompType>::weights3[xyz] * DGMatrices<pCompType>::FLCoeff[t];
      #ifdef SharedOMP
      #pragma omp simd aligned(rhs,luh:{{ALIGNMENT}})
      #endif
      for (int n = 0; n < {{NUMBER_OF_UNKNOWNS}}; n++) {
        rhs[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * (xyz + {{NUMBER_OF_DOF**DIMENSIONS}} * t)] = weight * luh[n + {{NUMBER_OF_DATA}} * xyz];
      }
    }

{% if USE_FLUX %}
    // Compute the "derivatives" (contributions of the stiffness matrix)
    // x-direction (independent from the y- and z-derivatives)
    for (int z = 0; z < {{NUMBER_OF_DOF_3D}}; z++) {
      for (int y = 0; y < {{NUMBER_OF_DOF}}; y++) {
        {{ m.gemm('rhs_x', 'lFi', 'coeffRhsX', 'rhs',
          '((t*'~NUMBER_OF_DOF_3D~'+z)*'~NUMBER_OF_DOF~'+y)*'~NUMBER_OF_UNKNOWNS_PADDED*NUMBER_OF_DOF,
          '0',
          '((t*'~NUMBER_OF_DOF_3D~'+z)*'~NUMBER_OF_DOF~'+y)*'~NUMBER_OF_UNKNOWNS_PADDED*NUMBER_OF_DOF,
          true_B='Kxi',
          true_alpha='-Quadrature<pCompType>::weights3[t*'~NUMBER_OF_DOF*NUMBER_OF_DOF_3D~'+z*'~NUMBER_OF_DOF~'+y] * dtByDx') | indent(10) }}{##}
      }
    }

    // y-direction (independent from the x- and z-derivatives)
    for (int z = 0; z < {{NUMBER_OF_DOF_3D}}; z++) {
      for (int x = 0; x < {{NUMBER_OF_DOF}}; x++) {
        {{ m.gemm('rhs_y', 'lFi', 'coeffRhsY', 'rhs',
          '((t*'~NUMBER_OF_DOF_3D~'+z)*'~NUMBER_OF_DOF*NUMBER_OF_DOF~'+x)*'~NUMBER_OF_UNKNOWNS_PADDED~'+'~1*(NUMBER_OF_DOF**DIMENSIONS)*NUMBER_OF_DOF*NUMBER_OF_UNKNOWNS_PADDED,
          '0',
          '((t*'~NUMBER_OF_DOF_3D~'+z)*'~NUMBER_OF_DOF*NUMBER_OF_DOF~'+x)*'~NUMBER_OF_UNKNOWNS_PADDED,
          true_B='Kxi',
          true_alpha='-Quadrature<pCompType>::weights3[t*'~NUMBER_OF_DOF*NUMBER_OF_DOF_3D~'+z*'~NUMBER_OF_DOF~'+x] * dtByDx') | indent(10) }}{##}
      }
    }

{% if DIMENSIONS==3 %}
    // z-direction (independent from the x- and y-derivatives)
    for (int y = 0; y < {{NUMBER_OF_DOF}}; y++) {
      for (int x = 0; x < {{NUMBER_OF_DOF}}; x++) {
        {{ m.gemm('rhs_z', 'lFi','coeffRhsZ', 'rhs',
          '((t*'~NUMBER_OF_DOF*NUMBER_OF_DOF~'+y)*'~NUMBER_OF_DOF~'+x)*'~NUMBER_OF_UNKNOWNS_PADDED~'+'~2*(NUMBER_OF_DOF**DIMENSIONS)*NUMBER_OF_DOF*NUMBER_OF_UNKNOWNS_PADDED,
          '0',
          '((t*'~NUMBER_OF_DOF*NUMBER_OF_DOF~'+y)*'~NUMBER_OF_DOF~'+x)*'~NUMBER_OF_UNKNOWNS_PADDED,
          true_B='Kxi',
          true_alpha='-Quadrature<pCompType>::weights3[t*'~NUMBER_OF_DOF*NUMBER_OF_DOF_3D~'+y*'~NUMBER_OF_DOF~'+x] * dtByDx') | indent(10) }}{##}
      }
    }
{% endif %}
{% endif %}{# USE_FLUX #}

{% if USE_NCP or USE_SOURCE %}
{% if USE_NCP %}
    pCompType tmpNCP[{{NUMBER_OF_UNKNOWNS_PADDED}}] __attribute__((aligned({{ALIGNMENT}})));
    pCompType gradQ_PDE[{{NUMBER_OF_UNKNOWNS*DIMENSIONS}}] __attribute__((aligned({{ALIGNMENT}})));
{% endif %}

    for (int xyz = 0; xyz < {{NUMBER_OF_DOF**DIMENSIONS}}; xyz++) {
{% if USE_NCP %}
      std::copy_n(gradQ + {{NUMBER_OF_UNKNOWNS_PADDED}} * xyz, {{NUMBER_OF_UNKNOWNS}}, gradQ_PDE);
      std::copy_n(gradQ + {{NUMBER_OF_UNKNOWNS_PADDED}} * xyz + {{1*NUMBER_OF_UNKNOWNS_PADDED*(NUMBER_OF_DOF**DIMENSIONS)}}, {{NUMBER_OF_UNKNOWNS}}, gradQ_PDE + {{1*NUMBER_OF_UNKNOWNS}});
{% if DIMENSIONS==3 %}
      std::copy_n(gradQ + {{NUMBER_OF_UNKNOWNS_PADDED}} * xyz + {{2*NUMBER_OF_UNKNOWNS_PADDED*(NUMBER_OF_DOF**DIMENSIONS)}}, {{NUMBER_OF_UNKNOWNS}}, gradQ_PDE + {{2*NUMBER_OF_UNKNOWNS}});
{% endif %}
{% endif %}

{% if USE_SOURCE %}
      solver.algebraicSource(
        lQi + (xyz * {{NUMBER_OF_DOF}} + t) * {{NUMBER_OF_DATA_PADDED}},
        cellCentre,
        cellSize,
        timeStamp,
        timeStepSize,
        lSi + (t * {{NUMBER_OF_DOF ** DIMENSIONS}} + xyz) * {{NUMBER_OF_UNKNOWNS_PADDED}}
      );
{% else %}
      std::fill_n(&lSi[0 + (t * {{NUMBER_OF_DOF ** DIMENSIONS}} + xyz) * {{NUMBER_OF_UNKNOWNS_PADDED}}], {{NUMBER_OF_UNKNOWNS_PADDED}}, 0.0);
{% endif %}

{% if USE_NCP %}
      for (int d = 0; d < Dimensions; d++) {
        solver.nonconservativeProduct(
          lQi + (t * {{NUMBER_OF_DOF**DIMENSIONS}} + xyz) * {{NUMBER_OF_DATA_PADDED}},
          gradQ_PDE + {{NUMBER_OF_UNKNOWNS_PADDED}} * d,
          cellCentre,
          cellSize,
          timeStamp,
          timeStepSize,
          d,
          tmpNCP
        );

        #ifdef SharedOMP
        #pragma omp simd aligned(lSi,tmpNCP:{{ALIGNMENT}})
        #endif
        for(int n = 0; n < {{NUMBER_OF_UNKNOWNS_PADDED}}; n++) {
          lSi[n + (t * {{NUMBER_OF_DOF ** DIMENSIONS}} + xyz) * {{NUMBER_OF_UNKNOWNS_PADDED}}] -= tmpNCP[n];
        }
      }
{% endif %}

      // Update rhs
      const pCompType updateSize = Quadrature<pCompType>::weights1[t] * Quadrature<pCompType>::weights3[xyz] * timeStepSize;
      #ifdef SharedOMP
      #pragma omp simd aligned(rhs,lSi:{{ALIGNMENT}})
      #endif
      for (int n = 0; n < {{NUMBER_OF_UNKNOWNS_PADDED}}; n++) {
        rhs[n + (t * {{NUMBER_OF_DOF ** DIMENSIONS}} + xyz) * {{NUMBER_OF_UNKNOWNS_PADDED}}]
          += updateSize * lSi[n + (t * {{NUMBER_OF_DOF ** DIMENSIONS}} + xyz) * {{NUMBER_OF_UNKNOWNS_PADDED}}];
      }
    }
{% endif %}{# USE_NCP or USE_SOURCE #}
  } // t

  // 3: Multiply with (K1)^(-1) to get the discrete time integral of the discrete Picard iteration
  pCompType sqRes = 0.0;
  for (int xyz = 0; xyz < {{NUMBER_OF_DOF**DIMENSIONS}}; xyz++) {
    {{ m.gemm('lqi', 'rhs', 's_m_QSlice', 'new_lQi_slice',
      NUMBER_OF_UNKNOWNS_PADDED~'*xyz', '0', '0',
      true_B='iK1_T',
      true_alpha='Quadrature<pCompType>::iweights3[xyz]') | indent(6) }}{##}

    for (int t = 0; t < {{NUMBER_OF_DOF}}; t++) {
      for (int n = 0; n< {{NUMBER_OF_UNKNOWNS}}; n++) {
        sqRes += (new_lQi_slice[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * t] - lQi[n + {{NUMBER_OF_DATA_PADDED}} * (xyz + {{NUMBER_OF_DOF**DIMENSIONS}} * t)])
          * (new_lQi_slice[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * t] - lQi[n + {{NUMBER_OF_DATA_PADDED}} * (xyz + {{NUMBER_OF_DOF**DIMENSIONS}} * t)]);

        lQi[n + {{NUMBER_OF_DATA_PADDED}} * (xyz + {{NUMBER_OF_DOF**DIMENSIONS}} * t)] = new_lQi_slice[n + {{NUMBER_OF_UNKNOWNS_PADDED}} * t];
      }
    }
  }

  return sqRes; // TODO: Is this the wrong return statement? We expect an integer to be returned. Is this return value used at all?
}

{% for PRECISION_NUM in range(0,PREDICTOR_COMPUTATION_PRECISIONS|length) %}
template {{PREDICTOR_COMPUTATION_PRECISIONS[PRECISION_NUM]}} {{FULL_QUALIFIED_NAMESPACE}}::picardIteration(
  {{FULL_QUALIFIED_SOLVER_NAME}}&                                         solver,
  {{PREDICTOR_COMPUTATION_PRECISIONS[PRECISION_NUM]}}* __restrict__       lQi,
  {{PREDICTOR_COMPUTATION_PRECISIONS[PRECISION_NUM]}}* __restrict__       rhs,
  {{PREDICTOR_COMPUTATION_PRECISIONS[PRECISION_NUM]}}* __restrict__       lFi,
  {{PREDICTOR_COMPUTATION_PRECISIONS[PRECISION_NUM]}}* __restrict__       lSi,
  {{PREDICTOR_COMPUTATION_PRECISIONS[PRECISION_NUM]}}* __restrict__       gradQ,
  {{PREDICTOR_COMPUTATION_PRECISIONS[PRECISION_NUM]}}* __restrict__       new_lQi_slice,
  const {{PREDICTOR_COMPUTATION_PRECISIONS[PRECISION_NUM]}}* __restrict__ dudxT_by_dx,
  const {{SOLUTION_STORAGE_PRECISION}}* const __restrict__                luh,
  const tarch::la::Vector<Dimensions, double>&                            cellCentre,
  const tarch::la::Vector<Dimensions, double>&                            cellSize,
  const double                                                            timeStamp,
  const double                                                            timeStepSize
);
{% endfor %}
