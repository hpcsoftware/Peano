// **********************************************************************************************
// ***                                     !!!WARNING!!!                                      ***
// *** WARNING: AUTO GENERATED FILE! DO NOT MODIFY BY HAND! YOUR CHANGES WILL BE OVERWRITTEN! ***
// ***                                     !!!WARNING!!!                                      ***
// ***                  Generated by Peano's Python API: www.peano-framework.org              ***
// **********************************************************************************************
#include "Quadrature.h"

#include <mm_malloc.h>

// Use {{POLYNOMIAL_TYPE}} quadrature

template <typename T> T* {{FULL_QUALIFIED_NAMESPACE}}::Quadrature<T>::nodes;
template <typename T> T* {{FULL_QUALIFIED_NAMESPACE}}::Quadrature<T>::uh2lob;
template <typename T> T* {{FULL_QUALIFIED_NAMESPACE}}::Quadrature<T>::dg2fv;
template <typename T> T* {{FULL_QUALIFIED_NAMESPACE}}::Quadrature<T>::fv2dg;

template class {{FULL_QUALIFIED_NAMESPACE}}::Quadrature<double>;

template <>
void {{FULL_QUALIFIED_NAMESPACE}}::Quadrature<double>::initQuadratureNodesAndWeights() {
  nodes     = (double *) _mm_malloc(sizeof(double) * {{NUMBER_OF_DOF_PADDED}}, {{ALIGNMENT}});
  uh2lob    = (double *) _mm_malloc(sizeof(double) * {{uh2lobSize}}, {{ALIGNMENT}}); //nDof*nDofPad
  dg2fv     = (double *) _mm_malloc(sizeof(double) * {{dg2fvSize}}, {{ALIGNMENT}}); //nDof*nDofLimPad
  fv2dg     = (double *) _mm_malloc(sizeof(double) * {{fv2dgSize}}, {{ALIGNMENT}}); //nDofLim*nDofPad

{% for i in quadrature_seq %}
  nodes[{{i}}]     = (double) {{"{:.15e}".format(QuadratureNodes[i])}};
{% endfor %}

{% for i in uh2lob_seq %}
  uh2lob[{{i}}]    = (double) {{"{:.15e}".format(uh2lob[i])}};
{% endfor %}

{% for i in dg2fv_seq %}
  dg2fv[{{i}}]    = (double) {{"{:.15e}".format(dg2fv[i])}};
{% endfor %}

{% for i in fv2dg_seq %}
  fv2dg[{{i}}]    = (double) {{"{:.15e}".format(fv2dg[i])}};
{% endfor %}
}

template <>
void {{FULL_QUALIFIED_NAMESPACE}}::Quadrature<double>::freeQuadratureNodesAndWeights() {
  _mm_free(uh2lob);
  _mm_free(dg2fv);
  _mm_free(fv2dg);
}
