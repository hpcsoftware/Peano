// This file is part of the ExaHyPE2 project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org

#include <iomanip>
#include "config.h"

#include "{{MAIN_NAME}}.h"

#include "exahype2/UnitTests.h"
#include "exahype2/UserInterface.h"
#include "peano4/peano.h"
#include "peano4/UnitTests.h"
#include "peano4/grid/Spacetree.h"
#include "peano4/parallel/SpacetreeSet.h"

#include "{{SUBDIRECTORY}}Constants.h"
#include "{{SUBDIRECTORY}}observers/CreateGrid.h"
#include "{{SUBDIRECTORY}}observers/CreateGridAndConvergeLoadBalancing.h"
#include "{{SUBDIRECTORY}}observers/CreateGridButPostponeRefinement.h"
#include "{{SUBDIRECTORY}}observers/InitGrid.h"
#include "{{SUBDIRECTORY}}observers/PlotSolution.h"
#include "{{SUBDIRECTORY}}observers/CheckpointSolution.h"
#include "{{SUBDIRECTORY}}observers/TimeStep.h"
{% if SUBDIRECTORIES -%}
{% for subdirectory in SUBDIRECTORIES %}
#include "{{subdirectory}}repositories/DataRepository.h"
#include "{{subdirectory}}repositories/SolverRepository.h"
#include "{{subdirectory}}repositories/StepRepository.h"
{% endfor -%}
{% else %}
#include "repositories/DataRepository.h"
#include "repositories/SolverRepository.h"
#include "repositories/StepRepository.h"
{%- endif %}

#include "tarch/UnitTests.h"
#include "tarch/logging/Log.h"
#include "tarch/logging/LogFilter.h"
#include "tarch/logging/Statistics.h"
#include "tarch/multicore/Core.h"
#include "tarch/multicore/multicore.h"
#include "tarch/multicore/otter.h"
#include "tarch/NonCriticalAssertions.h"
#include "tarch/tests/TreeTestCaseCollection.h"
#include "tarch/timing/Measurement.h"
#include "tarch/timing/Watch.h"
#include "toolbox/blockstructured/UnitTests.h"
#include "toolbox/loadbalancing/loadbalancing.h"


{% if FENV_ARGS is defined and FENV_ARGS != "" -%}
#include <fenv.h>
#pragma float_control(precise, on)
#pragma STDC FENV_ACCESS ON
{% endif -%}

using namespace {{NAMESPACE | join("::")}};

tarch::logging::Log _log("::");


tarch::timing::Measurement timePerMeshSweepMeasurement;
tarch::timing::Measurement gridConstructionMeasurement;
tarch::timing::Measurement timeStepMeasurement;
tarch::timing::Measurement plotMeasurement;


/**
 * Decide which step to run next
 *
 * ## Control of the parallel grid construction
 *
 *
 * @return continues to run
 */
bool selectNextAlgorithmicStep() {
  static bool                                   gridConstructed                       = false;
  static bool                                   gridInitialised                       = false;
  static bool                                   gridBalanced                          = false;
  static double                                 nextMaxPlotTimeStamp                  = {{SUBNAMESPACE}}FirstPlotTimeStamp;
  static double                                 nextMinPlotTimeStamp                  = {{SUBNAMESPACE}}FirstPlotTimeStamp;
  static double                                 nextMaxCheckpointTimeStamp            = {{SUBNAMESPACE}}FirstCheckpointTimeStamp;
  static double                                 nextMinCheckpointTimeStamp            = {{SUBNAMESPACE}}FirstCheckpointTimeStamp;
  static bool                                   haveJustWrittenSnapshot               = false;
  static bool                                   haveReceivedNoncriticialAssertion     = false;
  static bool                                   addGridSweepWithoutGridRefinementNext = false;
  static tarch::la::Vector<Dimensions, double>  minH                                  = tarch::la::Vector<Dimensions, double>(
                                                                                          std::numeric_limits<double>::max()
                                                                                        );
  static int                                    globalNumberOfTrees                   = 0;
  bool                                          continueToSolve                       = true;

  if (tarch::hasNonCriticalAssertionBeenViolated() and not haveReceivedNoncriticialAssertion) {
    peano4::parallel::Node::getInstance().setNextProgramStep(
      {{SUBNAMESPACE}}repositories::StepRepository::toProgramStep({{SUBNAMESPACE}}repositories::StepRepository::Steps::PlotSolution)
    );
    haveReceivedNoncriticialAssertion = true;
    logError(
      "selectNextAlgorithmicStep()", "non-critical assertion has been triggered in code. Dump final state and terminate"
    );
  } else if (tarch::hasNonCriticalAssertionBeenViolated()) {
    continueToSolve = false;
  } else if (gridConstructed and not gridBalanced) {
    if (not {{SUBNAMESPACE}}repositories::loadBalancer.isEnabled(true) and not {{SUBNAMESPACE}}repositories::loadBalancer.hasSplitRecently()) {
      logInfo("selectNextAlgorithmicStep()", "all ranks have switched off their load balancing");
      gridBalanced = true;
    } else {
      logInfo(
        "selectNextAlgorithmicStep()", "wait for load balancing to become stable: " << {{SUBNAMESPACE}}repositories::loadBalancer
      );
    }

    peano4::parallel::Node::getInstance().setNextProgramStep({{SUBNAMESPACE}}repositories::StepRepository::toProgramStep(
      {{SUBNAMESPACE}}repositories::StepRepository::Steps::CreateGridAndConvergeLoadBalancing
    ));
  } else if (gridBalanced and not gridInitialised) {
    peano4::parallel::Node::getInstance().setNextProgramStep(
      {{SUBNAMESPACE}}repositories::StepRepository::toProgramStep({{SUBNAMESPACE}}repositories::StepRepository::Steps::InitGrid)
    );

    gridInitialised = true;
  } else if (not gridConstructed) {
    if (tarch::la::max(peano4::parallel::SpacetreeSet::getInstance().getGridStatistics().getMinH()) < tarch::la::max(minH)) {
      minH = peano4::parallel::SpacetreeSet::getInstance().getGridStatistics().getMinH();
      logInfo("selectNextAlgorithmicStep()", "mesh has refined, so reset minH to" << minH << " and postpone further refinement");
      addGridSweepWithoutGridRefinementNext = true;
    } else if ({{SUBNAMESPACE}}repositories::loadBalancer.getGlobalNumberOfTrees() > globalNumberOfTrees) {
      logInfo("selectNextAlgorithmicStep()", "mesh has rebalanced recently, so postpone further refinement");
      addGridSweepWithoutGridRefinementNext = true;
      globalNumberOfTrees                   = {{SUBNAMESPACE}}repositories::loadBalancer.getGlobalNumberOfTrees();
    }
    else if (
      peano4::parallel::SpacetreeSet::getInstance().getGridStatistics().getStationarySweeps()>5
      and
      // ensure that a proper creation has been ran before, so the mesh had the opportunity
      // to refine further if it has not done yet
      {{SUBNAMESPACE}}repositories::StepRepository::toStepEnum( peano4::parallel::Node::getInstance().getCurrentProgramStep() ) == {{SUBNAMESPACE}}repositories::StepRepository::Steps::CreateGrid
    ) {
      logInfo(
        "selectNextAlgorithmicStep()", "grid has been stationary for quite some time. Terminate grid construction"
      );
      addGridSweepWithoutGridRefinementNext = false;
      gridConstructed                       = true;
    } else {
      logInfo(
        "selectNextAlgorithmicStep()",
        "mesh rebalancing seems to be stationary, so study whether to refine mesh further in next sweep: "
          << peano4::parallel::SpacetreeSet::getInstance().getGridStatistics().toString()
      );
      addGridSweepWithoutGridRefinementNext = false;
      globalNumberOfTrees                   = {{SUBNAMESPACE}}repositories::loadBalancer.getGlobalNumberOfTrees();
    }

    // Actual grid traversal choice
    if (addGridSweepWithoutGridRefinementNext) {
      peano4::parallel::Node::getInstance().setNextProgramStep({{SUBNAMESPACE}}repositories::StepRepository::toProgramStep(
        {{SUBNAMESPACE}}repositories::StepRepository::Steps::CreateGridButPostponeRefinement
      ));
    } else {
      peano4::parallel::Node::getInstance().setNextProgramStep(
        {{SUBNAMESPACE}}repositories::StepRepository::toProgramStep({{SUBNAMESPACE}}repositories::StepRepository::Steps::CreateGrid)
      );
    }

    continueToSolve = true;
  } else {
    if ({{SUBNAMESPACE}}TimeInBetweenPlots > 0.0 and {{SUBNAMESPACE}}repositories::getMinTimeStamp() < {{SUBNAMESPACE}}MinTerminalTime and {{SUBNAMESPACE}}repositories::getMaxTimeStamp() < {{SUBNAMESPACE}}MaxTerminalTime and ({{SUBNAMESPACE}}repositories::getMinTimeStamp() >= nextMinPlotTimeStamp or {{SUBNAMESPACE}}repositories::getMaxTimeStamp() >= nextMaxPlotTimeStamp) and {{SUBNAMESPACE}}repositories::mayPlot()) {
      if ({{SUBNAMESPACE}}repositories::getMinTimeStamp() >= nextMinPlotTimeStamp) {
        nextMinPlotTimeStamp += {{SUBNAMESPACE}}TimeInBetweenPlots;
      }
      if ({{SUBNAMESPACE}}repositories::getMaxTimeStamp() >= nextMaxPlotTimeStamp) {
        nextMaxPlotTimeStamp += {{SUBNAMESPACE}}TimeInBetweenPlots;
      }

      if (nextMinPlotTimeStamp < {{SUBNAMESPACE}}repositories::getMinTimeStamp()) {
        logWarning(
          "selectNextAlgorithmicStep()",
          "code is asked to plot every dt="
            << {{SUBNAMESPACE}}TimeInBetweenPlots << ", but this seems to be less than the time step size of the solvers. "
            << "So postpone next plot to t=" << ({{SUBNAMESPACE}}repositories::getMinTimeStamp() + {{SUBNAMESPACE}}TimeInBetweenPlots)
        );
        nextMinPlotTimeStamp = {{SUBNAMESPACE}}repositories::getMinTimeStamp() + {{SUBNAMESPACE}}TimeInBetweenPlots;
      } else if (nextMaxPlotTimeStamp < {{SUBNAMESPACE}}repositories::getMaxTimeStamp()) {
        logWarning(
          "selectNextAlgorithmicStep()",
          "code is asked to plot every dt="
            << {{SUBNAMESPACE}}TimeInBetweenPlots << ", but this seems to be less than the time step size of the solvers. "
            << "So postpone next plot to t=" << ({{SUBNAMESPACE}}repositories::getMaxTimeStamp() + {{SUBNAMESPACE}}TimeInBetweenPlots)
        );
        nextMaxPlotTimeStamp = {{SUBNAMESPACE}}repositories::getMaxTimeStamp() + {{SUBNAMESPACE}}TimeInBetweenPlots;
      }

      nextMaxPlotTimeStamp = std::max(nextMaxPlotTimeStamp, nextMinPlotTimeStamp);

      peano4::parallel::Node::getInstance().setNextProgramStep(
        {{SUBNAMESPACE}}repositories::StepRepository::toProgramStep({{SUBNAMESPACE}}repositories::StepRepository::Steps::PlotSolution)
      );
      haveJustWrittenSnapshot = true;
      continueToSolve         = true;
    } else if ({{SUBNAMESPACE}}TimeInBetweenCheckpoints > 0.0 and {{SUBNAMESPACE}}repositories::getMinTimeStamp() < {{SUBNAMESPACE}}MinTerminalTime and {{SUBNAMESPACE}}repositories::getMaxTimeStamp() < {{SUBNAMESPACE}}MaxTerminalTime and ({{SUBNAMESPACE}}repositories::getMinTimeStamp() >= nextMinCheckpointTimeStamp or {{SUBNAMESPACE}}repositories::getMaxTimeStamp() >= nextMaxCheckpointTimeStamp) and {{SUBNAMESPACE}}repositories::mayPlot()) {
      if ({{SUBNAMESPACE}}repositories::getMinTimeStamp() >= nextMinCheckpointTimeStamp) {
        nextMinCheckpointTimeStamp += {{SUBNAMESPACE}}TimeInBetweenCheckpoints;
      }
      if ({{SUBNAMESPACE}}repositories::getMaxTimeStamp() >= nextMaxCheckpointTimeStamp) {
        nextMaxCheckpointTimeStamp += {{SUBNAMESPACE}}TimeInBetweenCheckpoints;
      }

      if (nextMinCheckpointTimeStamp < {{SUBNAMESPACE}}repositories::getMinTimeStamp()) {
        logWarning(
          "selectNextAlgorithmicStep()",
          "code is asked to Checkpoint every dt="
            << {{SUBNAMESPACE}}TimeInBetweenCheckpoints << ", but this seems to be less than the time step size of the solvers. "
            << "So postpone next Checkpoint to t=" << ({{SUBNAMESPACE}}repositories::getMinTimeStamp() + {{SUBNAMESPACE}}TimeInBetweenCheckpoints)
        );
        nextMinCheckpointTimeStamp = {{SUBNAMESPACE}}repositories::getMinTimeStamp() + {{SUBNAMESPACE}}TimeInBetweenCheckpoints;
      } else if (nextMaxCheckpointTimeStamp < {{SUBNAMESPACE}}repositories::getMaxTimeStamp()) {
        logWarning(
          "selectNextAlgorithmicStep()",
          "code is asked to Checkpoint every dt="
            << {{SUBNAMESPACE}}TimeInBetweenCheckpoints << ", but this seems to be less than the time step size of the solvers. "
            << "So postpone next Checkpoint to t=" << ({{SUBNAMESPACE}}repositories::getMaxTimeStamp() + {{SUBNAMESPACE}}TimeInBetweenCheckpoints)
        );
        nextMaxCheckpointTimeStamp = {{SUBNAMESPACE}}repositories::getMaxTimeStamp() + {{SUBNAMESPACE}}TimeInBetweenCheckpoints;
      }

      nextMaxCheckpointTimeStamp = std::max(nextMaxCheckpointTimeStamp, nextMinCheckpointTimeStamp);

      peano4::parallel::Node::getInstance().setNextProgramStep(
        {{SUBNAMESPACE}}repositories::StepRepository::toProgramStep({{SUBNAMESPACE}}repositories::StepRepository::Steps::CheckpointSolution)
      );
      haveJustWrittenSnapshot = true;
      continueToSolve         = true;
    } else if ({{SUBNAMESPACE}}repositories::getMinTimeStamp() < {{SUBNAMESPACE}}MinTerminalTime and {{SUBNAMESPACE}}repositories::getMaxTimeStamp() < {{SUBNAMESPACE}}MaxTerminalTime) {
      peano4::parallel::Node::getInstance().setNextProgramStep(
        {{SUBNAMESPACE}}repositories::StepRepository::toProgramStep({{SUBNAMESPACE}}repositories::StepRepository::Steps::TimeStep)
      );
      continueToSolve         = true;
      haveJustWrittenSnapshot = false;
    } else {
      if (not haveJustWrittenSnapshot and {{SUBNAMESPACE}}TimeInBetweenPlots > 0.0 and {{SUBNAMESPACE}}repositories::mayPlot()) {
        peano4::parallel::Node::getInstance().setNextProgramStep(
          {{SUBNAMESPACE}}repositories::StepRepository::toProgramStep({{SUBNAMESPACE}}repositories::StepRepository::Steps::PlotSolution)
        );
        continueToSolve         = true; // don't want to terminate immediately
        haveJustWrittenSnapshot = true;
        nextMinPlotTimeStamp    = std::numeric_limits<double>::max();
        nextMaxPlotTimeStamp    = std::numeric_limits<double>::max();
      } else if (not haveJustWrittenSnapshot and {{SUBNAMESPACE}}TimeInBetweenPlots > 0.0 and not {{SUBNAMESPACE}}repositories::mayPlot()) {
        continueToSolve = true; // don't want to terminate immediately but to wait for incomplete time steps to complete
      } else {
        continueToSolve = false;
      }
    }
  }

  return continueToSolve;
}


void step() {
  int  stepIdentifier = peano4::parallel::Node::getInstance().getCurrentProgramStep();
  auto stepName       = {{SUBNAMESPACE}}repositories::StepRepository::toStepEnum(stepIdentifier);

  static tarch::logging::Log _log("");
#if PeanoDebug > 0
#else
  if (tarch::mpi::Rank::getInstance().isGlobalMaster())
#endif
  logInfo("step()", "Starting AlgorithmicStep [" << {{SUBNAMESPACE}}repositories::StepRepository::toString(stepName)<<"]" );

  static tarch::timing::Watch watch("::", "step()", false);

  static int creepingNumberOfLocalCells = 0;

  switch (stepName) {
  case {{SUBNAMESPACE}}repositories::StepRepository::Steps::CreateGridButPostponeRefinement: {
    tarch::logging::LogFilter::getInstance().switchProgramPhase("create-grid-but-postpone-refinement");

    OTTER_PHASE_SWITCH(otter::phase::create_grid_no_refine);

    {{SUBNAMESPACE}}repositories::startGridConstructionStep();

    OTTER_DEFINE_TASK(step_task, OTTER_NULL_TASK, otter_no_add_to_pool, otter::label::step);
    OTTER_TASK_START(step_task);

    {{SUBNAMESPACE}}observers::CreateGridButPostponeRefinement observer;
    watch.start();
    peano4::parallel::SpacetreeSet::getInstance().traverse(observer);
    watch.stop();
    gridConstructionMeasurement.setValue(watch.getCalendarTime());

    OTTER_TASK_WAIT_IMPLICIT(children);
    OTTER_TASK_END(step_task);
    OTTER_TASK_WAIT_IMPLICIT(descendants);

    {{SUBNAMESPACE}}repositories::finishGridConstructionStep();
  } break;
  case {{SUBNAMESPACE}}repositories::StepRepository::Steps::CreateGrid: {
    tarch::logging::LogFilter::getInstance().switchProgramPhase("create-grid");

    OTTER_PHASE_SWITCH(otter::phase::create_grid);

    {{SUBNAMESPACE}}repositories::startGridConstructionStep();

    OTTER_DEFINE_TASK(step_task, OTTER_NULL_TASK, otter_no_add_to_pool, otter::label::step);
    OTTER_TASK_START(step_task);

    {{SUBNAMESPACE}}observers::CreateGrid observer;
    watch.start();
    peano4::parallel::SpacetreeSet::getInstance().traverse(observer);
    watch.stop();
    gridConstructionMeasurement.setValue(watch.getCalendarTime());

    OTTER_TASK_WAIT_IMPLICIT(children);
    OTTER_TASK_END(step_task);
    OTTER_TASK_WAIT_IMPLICIT(descendants);

    {{SUBNAMESPACE}}repositories::finishGridConstructionStep();

    // We always overestimate so give the convergence the opportunity to catch up. The constant
    // here is a magic one.
    creepingNumberOfLocalCells = ::toolbox::loadbalancing::getWeightOfHeaviestLocalSpacetree()
                                 + tarch::multicore::Core::getInstance().getNumberOfThreads() * 3;
  } break;
  case {{SUBNAMESPACE}}repositories::StepRepository::Steps::CreateGridAndConvergeLoadBalancing: {
    if (creepingNumberOfLocalCells < ::toolbox::loadbalancing::getWeightOfHeaviestLocalSpacetree() - 1) {
      logInfo(
        "step()",
        "it seems the grid has just refined before we switched to the phase where we make the load balancing converge. Wait for a few iterations more to give load balancing chance to catch up"
      );
      creepingNumberOfLocalCells = ::toolbox::loadbalancing::getWeightOfHeaviestLocalSpacetree()
                                   + tarch::multicore::Core::getInstance().getNumberOfThreads() * 3;
    }

    tarch::logging::LogFilter::getInstance().switchProgramPhase("create-grid-and-converge-load-balancing");

    OTTER_PHASE_SWITCH(otter::phase::create_grid_converge);

    // The smaller here corresponds to the -1 below
    if (::toolbox::loadbalancing::getWeightOfHeaviestLocalSpacetree() < 0 and {{SUBNAMESPACE}}repositories::loadBalancer.isEnabled(false)) {
      logInfo("step()", "rank is degenerated so disable load balancing temporarily");
      {{SUBNAMESPACE}}repositories::loadBalancer.enable(false);
    }
    if (
          ::toolbox::loadbalancing::getWeightOfHeaviestLocalSpacetree() >= creepingNumberOfLocalCells
          and
          {{SUBNAMESPACE}}repositories::loadBalancer.isEnabled(false)
        ) {
      logInfo(
        "step()",
        "grid construction and decomposition on this rank seem to be stable as we have around "
          << creepingNumberOfLocalCells << " local cells in the heaviest tree. Disable load balancing temporarily"
      );
      {{SUBNAMESPACE}}repositories::loadBalancer.enable(false);
    }

    {{SUBNAMESPACE}}repositories::startGridConstructionStep();

    OTTER_DEFINE_TASK(step_task, OTTER_NULL_TASK, otter_no_add_to_pool, otter::label::step);
    OTTER_TASK_START(step_task);

    {{SUBNAMESPACE}}observers::CreateGridButPostponeRefinement observer;
    watch.start();
    peano4::parallel::SpacetreeSet::getInstance().traverse(observer);
    watch.stop();
    gridConstructionMeasurement.setValue(watch.getCalendarTime());

    OTTER_TASK_WAIT_IMPLICIT(children);
    OTTER_TASK_END(step_task);
    OTTER_TASK_WAIT_IMPLICIT(descendants);

    {{SUBNAMESPACE}}repositories::finishGridConstructionStep();

    if (
          ::toolbox::loadbalancing::getWeightOfHeaviestLocalSpacetree() <= creepingNumberOfLocalCells
          and
          not {{SUBNAMESPACE}}repositories::loadBalancer.hasSplitRecently()
          and
          {{SUBNAMESPACE}}repositories::loadBalancer.isEnabled(false)
        ) {
      logInfo(
        "step()",
        "have to decrement local cell counter "
          << creepingNumberOfLocalCells << " as maximum weight is "
          << ::toolbox::loadbalancing::getWeightOfHeaviestLocalSpacetree()
      );
      creepingNumberOfLocalCells = (creepingNumberOfLocalCells
                                    + ::toolbox::loadbalancing::getWeightOfHeaviestLocalSpacetree())
                                   / 2;
    }
  } break;
  case {{SUBNAMESPACE}}repositories::StepRepository::Steps::InitGrid: {
    tarch::logging::LogFilter::getInstance().switchProgramPhase("init-grid");
    {{SUBNAMESPACE}}repositories::loadBalancer.enable(false);

    OTTER_PHASE_SWITCH(otter::phase::init);

    {{SUBNAMESPACE}}repositories::startGridInitialisationStep();

    OTTER_DEFINE_TASK(step_task, OTTER_NULL_TASK, otter_no_add_to_pool, otter::label::step);
    OTTER_TASK_START(step_task);

    {{SUBNAMESPACE}}observers::InitGrid observer;
    {{SUBNAMESPACE}}observers::InitGrid::prepareTraversal();
    watch.start();
    peano4::parallel::SpacetreeSet::getInstance().traverse(observer);
    watch.stop();
    {{SUBNAMESPACE}}observers::InitGrid::unprepareTraversal();
    gridConstructionMeasurement.setValue(watch.getCalendarTime());

    OTTER_TASK_WAIT_IMPLICIT(children);
    OTTER_TASK_END(step_task);
    OTTER_TASK_WAIT_IMPLICIT(descendants);

    {{SUBNAMESPACE}}repositories::finishGridInitialisationStep();
  } break;
  case {{SUBNAMESPACE}}repositories::StepRepository::Steps::PlotSolution: {
    tarch::logging::LogFilter::getInstance().switchProgramPhase("plot-solution");
    const double minTimeStamp    = {{SUBNAMESPACE}}repositories::getMinTimeStamp();
    const double maxTimeStamp    = {{SUBNAMESPACE}}repositories::getMaxTimeStamp();
    const double minTimeStepSize = {{SUBNAMESPACE}}repositories::getMinTimeStepSize();
    const double maxTimeStepSize = {{SUBNAMESPACE}}repositories::getMaxTimeStepSize();

    OTTER_PHASE_SWITCH(otter::phase::plot);

    {{SUBNAMESPACE}}repositories::startPlottingStep(minTimeStamp, maxTimeStamp, minTimeStepSize, maxTimeStepSize);

    OTTER_DEFINE_TASK(step_task, OTTER_NULL_TASK, otter_no_add_to_pool, otter::label::step);
    OTTER_TASK_START(step_task);

    {{SUBNAMESPACE}}observers::PlotSolution observer;
    {{SUBNAMESPACE}}observers::PlotSolution::prepareTraversal();
    watch.start();
    peano4::parallel::SpacetreeSet::getInstance().traverse(observer);
    watch.stop();
    {{SUBNAMESPACE}}observers::PlotSolution::unprepareTraversal();
    plotMeasurement.setValue(watch.getCalendarTime());

    OTTER_TASK_WAIT_IMPLICIT(children);
    OTTER_TASK_END(step_task);
    OTTER_TASK_WAIT_IMPLICIT(descendants);

    {{SUBNAMESPACE}}repositories::finishPlottingStep();
  } break;
  case {{SUBNAMESPACE}}repositories::StepRepository::Steps::CheckpointSolution: {
    tarch::logging::LogFilter::getInstance().switchProgramPhase("Checkpoint-solution");
    const double minTimeStamp    = {{SUBNAMESPACE}}repositories::getMinTimeStamp();
    const double maxTimeStamp    = {{SUBNAMESPACE}}repositories::getMaxTimeStamp();
    const double minTimeStepSize = {{SUBNAMESPACE}}repositories::getMinTimeStepSize();
    const double maxTimeStepSize = {{SUBNAMESPACE}}repositories::getMaxTimeStepSize();

    OTTER_PHASE_SWITCH(otter::phase::plot);

    {{SUBNAMESPACE}}repositories::startPlottingStep(minTimeStamp, maxTimeStamp, minTimeStepSize, maxTimeStepSize);

    OTTER_DEFINE_TASK(step_task, OTTER_NULL_TASK, otter_no_add_to_pool, otter::label::step);
    OTTER_TASK_START(step_task);

    {{SUBNAMESPACE}}observers::CheckpointSolution observer;
    {{SUBNAMESPACE}}observers::CheckpointSolution::prepareTraversal();
    watch.start();
    peano4::parallel::SpacetreeSet::getInstance().traverse(observer);
    watch.stop();
    {{SUBNAMESPACE}}observers::CheckpointSolution::unprepareTraversal();
    plotMeasurement.setValue(watch.getCalendarTime());

    OTTER_TASK_WAIT_IMPLICIT(children);
    OTTER_TASK_END(step_task);
    OTTER_TASK_WAIT_IMPLICIT(descendants);

    {{SUBNAMESPACE}}repositories::finishPlottingStep();
  } break;
  case {{SUBNAMESPACE}}repositories::StepRepository::Steps::TimeStep: {
    tarch::logging::LogFilter::getInstance().switchProgramPhase("time-step");
    if ({{SUBNAMESPACE}}repositories::loadBalancer.isEnabled(false)) {
      logInfo("step()", "disable load balancing throughout initialisation (to be removed in later releases)");
      {{SUBNAMESPACE}}repositories::loadBalancer.enable(false);
    }

    const double minTimeStamp    = {{SUBNAMESPACE}}repositories::getMinTimeStamp();
    const double maxTimeStamp    = {{SUBNAMESPACE}}repositories::getMaxTimeStamp();
    const double minTimeStepSize = {{SUBNAMESPACE}}repositories::getMinTimeStepSize();
    const double maxTimeStepSize = {{SUBNAMESPACE}}repositories::getMaxTimeStepSize();
    const double minMeshSize     = {{SUBNAMESPACE}}repositories::getMinMeshSize();
    const double maxMeshSize     = {{SUBNAMESPACE}}repositories::getMaxMeshSize();

    OTTER_PHASE_SWITCH(otter::phase::timestep);

    {{SUBNAMESPACE}}repositories::startTimeStep(minTimeStamp, maxTimeStamp, minTimeStepSize, maxTimeStepSize);

    OTTER_DEFINE_TASK(step_task, OTTER_NULL_TASK, otter_no_add_to_pool, otter::label::step);
    OTTER_TASK_START(step_task);

    {{SUBNAMESPACE}}observers::TimeStep observer;
    {{SUBNAMESPACE}}observers::TimeStep::prepareTraversal();
    watch.start();
    peano4::parallel::SpacetreeSet::getInstance().traverse(observer);
    watch.stop();
    {{SUBNAMESPACE}}observers::TimeStep::unprepareTraversal();
    timeStepMeasurement.setValue(watch.getCalendarTime());

    OTTER_TASK_WAIT_IMPLICIT(children);
    OTTER_TASK_END(step_task);
    OTTER_TASK_WAIT_IMPLICIT(descendants);

    {{SUBNAMESPACE}}repositories::finishTimeStep();
  } break;
  case {{SUBNAMESPACE}}repositories::StepRepository::Steps::Undef:
    assertion(false);
    break;
  }
}

int main(int argc, char** argv) {
  constexpr int ExitCodeSuccess          = 0;
  constexpr int ExitCodeUnitTestsFailed  = 1;
  constexpr int ExitCodeInvalidArguments = 2;
  constexpr int ExitCodeInvalidBuild     = 3;

  static tarch::timing::Watch watch("::", "main()", false);

  peano4::initParallelEnvironment(&argc, &argv);

  // Do this early, so people can use logInfo properly.
  {{SUBNAMESPACE}}repositories::initLogFilters();

  tarch::initNonCriticalAssertionEnvironment();
  tarch::multicore::initSmartMPI();
  peano4::fillLookupTables();

  peano4::initSingletons({{SUBNAMESPACE}}DomainOffset, {{SUBNAMESPACE}}DomainSize, {{SUBNAMESPACE}}PeriodicBC);

  {{SUBNAMESPACE}}repositories::initSharedMemoryAndGPUEnvironment();

  if (tarch::mpi::Rank::getInstance().getNumberOfRanks() > 1 and tarch::multicore::Core::getInstance().getNumberOfThreads() <= 1) {
    logError("main()", "MPI runs without multithreading are not supported currently.");
    return ExitCodeInvalidBuild;
  }

  {{SUBNAMESPACE}}repositories::DataRepository::initDatatypes();

  {% if FENV_ARGS is defined and FENV_ARGS != "" -%}
  feenableexcept({{FENV_ARGS}} );
  {% endif -%}

#if PeanoDebug >= 2
  tarch::tests::TreeTestCaseCollection* unitTests = new tarch::tests::TreeTestCaseCollection();
  unitTests->addTestCase(peano4::getUnitTests());
  unitTests->addTestCase(tarch::getUnitTests());
  unitTests->addTestCase(toolbox::blockstructured::getUnitTests());
  unitTests->addTestCase(exahype2::getUnitTests());
  unitTests->run();
  if (unitTests->getNumberOfErrors() != 0) {
    logError("main()", "unit tests failed. Quit.");
    tarch::mpi::Rank::abort(ExitCodeUnitTestsFailed);
  }
  delete unitTests;
#endif

  {{SUBNAMESPACE}}repositories::startSimulation();

  tarch::logging::Statistics::getInstance().clear();

  OTTER_INITIALISE();

#if defined(SharedOMP)
#pragma omp parallel
  {
#pragma omp master
    {
#endif

#if defined(UseSmartMPI)
      const bool isGlobalMaster = tarch::mpi::Rank::getInstance().isGlobalMaster() and smartmpi::isComputeRank();
      const bool isPeanoComputeNode = not tarch::mpi::Rank::getInstance().isGlobalMaster() and smartmpi::isComputeRank();
#else
      const bool isGlobalMaster     = tarch::mpi::Rank::getInstance().isGlobalMaster();
      const bool isPeanoComputeNode = not tarch::mpi::Rank::getInstance().isGlobalMaster();
#endif

      if (isGlobalMaster) {
        while (selectNextAlgorithmicStep()) {
          watch.start();
          step();
          watch.stop();

          timePerMeshSweepMeasurement.setValue(watch.getCalendarTime());
          logInfo(
            "main()",
            "time per mesh sweep (current/average): " << std::fixed << std::setprecision(2) << watch.getCalendarTime() <<
            "s / " << timePerMeshSweepMeasurement.getValue() << "s"
          );
        }

        logInfo("main()", "terminated successfully");
        logInfo(
          "main()",
          "initial grid construction:   " << gridConstructionMeasurement.getAccumulatedValue() <<
          "s\t" << gridConstructionMeasurement.toString()
        );
        logInfo(
          "main()",
          "plotting:                    " << plotMeasurement.getAccumulatedValue() <<
          "s\t" << plotMeasurement.toString()
        );
        logInfo(
          "main()",
          "time stepping:               " << timeStepMeasurement.getAccumulatedValue() <<
          "s\t" << timeStepMeasurement.toString()
        );
        logInfo(
          "main()",
          "average time per mesh sweep: " << timePerMeshSweepMeasurement.getValue() <<
          "s\t" << timePerMeshSweepMeasurement.toString()
        );
      } else if (isPeanoComputeNode) {
        while (peano4::parallel::Node::getInstance().continueToRun()) {
          step();
        }
      }
#if defined(UseSmartMPI)
      else {
        while (smartmpi::continueToRun()) {
          smartmpi::tick();
        }
      }
#endif

#if defined(SharedOMP)
    }
  }
#endif

  tarch::logging::Statistics::getInstance().writeToCSV();

  OTTER_FINALISE();

  {{SUBNAMESPACE}}repositories::finishSimulation();

  peano4::shutdownSingletons();
  {{SUBNAMESPACE}}repositories::DataRepository::shutdownDatatypes();
  tarch::multicore::shutdownSmartMPI();
  tarch::shutdownNonCriticalAssertionEnvironment();
  peano4::shutdownParallelEnvironment();

  return ExitCodeSuccess;
}
