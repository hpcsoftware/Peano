#include "swift2/kernels/ParticleUpdatePredicates.h"
#include "peano4/utils/Loop.h"


#include <bit>
#include <list>


template <typename ParticleContainer>
void swift2::kernels::ompoffloading::forAllParticles(
    const peano4::datamanagement::VertexMarker&   marker,
    ParticleContainer&                            assignedParticles,
    int                                           numberOfAssignedParticles,
    ParticleUnaryOperatorOnVertex<typename ParticleContainer::DoFType>    auto f,
    UpdateParticleAssignedToVertexPredicate<typename ParticleContainer::DoFType> predicate
) {
  #if defined(GPUOffloadingOMP)
  @todo
  #else
  swift2::kernels::forAllParticles(
    marker,
    assignedParticles,
    numberOfAssignedParticles,
    f,
    predicate
  );
  #endif
}


template <typename ParticleContainer>
void swift2::kernels::ompoffloading::forAllParticles(
  const peano4::datamanagement::CellMarker&                    marker,
  ParticleContainer&                                           localParticles,
  const std::vector<int>&                                      numberOfLocalParticles,
  ParticleUnaryOperatorOnCell<PCParticle<ParticleContainer>>   auto f,
  UpdateParticleAssignedToCellPredicate<PCParticle<ParticleContainer>>
            predicate
) {
  #if defined(GPUOffloadingOMP)
  @todo
  #else
  swift2::kernels::forAllParticles(
    marker,
    localParticles,
    numberOfLocalParticles,
    f,
    predicate
  );
  #endif
}


template <typename LocalParticleContainer, typename ActiveParticleContainer>
void swift2::kernels::ompoffloading::forAllParticlePairs(
  const peano4::datamanagement::CellMarker&  marker,
  LocalParticleContainer&                    localParticles,
  ActiveParticleContainer&                   activeParticles,
  const std::vector<int>&                    numberOfLocalParticlesPerVertex,
  const std::vector<int>&                    numberOfActiveParticlesPerVertex,
  ParticleBinaryOperator<PCParticle<LocalParticleContainer>, PCParticle<ActiveParticleContainer>> auto f,
  UpdateParticleAssignedToCellPredicate<PCParticle<LocalParticleContainer>>
      localParticlePredicate,
  UpdateParticlePairWithinCellPredicate<PCParticle<LocalParticleContainer>>
      particlePairPredicate
) {
  #if defined(GPUOffloadingOMP)
  @todo
  #else
  swift2::kernels::forAllParticlePairs(
    marker,
    localParticles,
    activeParticles,
    numberOfLocalParticlesPerVertex,
    numberOfActiveParticlesPerVertex,
    f,
    localParticlePredicate,
    particlePairPredicate
  );
  #endif
}
