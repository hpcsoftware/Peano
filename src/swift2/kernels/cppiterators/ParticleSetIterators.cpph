#include "swift2/kernels/ParticleUpdatePredicates.h"
#include "peano4/utils/Loop.h"


#include <bit>
#include <list>


template <typename ParticleContainer>
void swift2::kernels::coalesced::forAllParticles(
    const peano4::datamanagement::VertexMarker&   marker,
    ParticleContainer&                            assignedParticles,
    int                                           numberOfAssignedParticles,
    ParticleUnaryOperatorOnVertex<typename ParticleContainer::DoFType>    auto f,
    UpdateParticleAssignedToVertexPredicate<typename ParticleContainer::DoFType> predicate
) {
  typename ParticleContainer::value_type pFirstParticle = *assignedParticles.begin();

  for (int i=0; i<numberOfAssignedParticles; i++) {
    PCParticle<ParticleContainer>&  particle = *(pFirstParticle+i);
    if (predicate(marker,particle)) {
      f(marker,particle);
    }
  }
}


template <typename ParticleContainer>
void swift2::kernels::coalesced::forAllParticles(
  const peano4::datamanagement::CellMarker&                    marker,
  ParticleContainer&                                           localParticles,
  const std::vector<int>&                                      numberOfLocalParticles,
  ParticleUnaryOperatorOnCell<PCParticle<ParticleContainer>>   auto f,
  UpdateParticleAssignedToCellPredicate<PCParticle<ParticleContainer>>
            predicate
) {
  typename ParticleContainer::value_type pFirstParticle = *localParticles.begin();

  for (auto& chunkSize: numberOfLocalParticles) {
    for (int i=0; i<chunkSize; i++) {
      PCParticle<ParticleContainer>&  particle = *(pFirstParticle+i);
      if (predicate(marker,particle)) {
        f(marker,particle);
      }
    }
    pFirstParticle += chunkSize;
  }
}


template <typename LocalParticleContainer, typename ActiveParticleContainer>
void swift2::kernels::coalesced::forAllParticlePairs(
  const peano4::datamanagement::CellMarker&  marker,
  LocalParticleContainer&                    localParticles,
  ActiveParticleContainer&                   activeParticles,
  const std::vector<int>&                    numberOfLocalParticlesPerVertex,
  const std::vector<int>&                    numberOfActiveParticlesPerVertex,
  ParticleBinaryOperator<PCParticle<LocalParticleContainer>, PCParticle<ActiveParticleContainer>> auto f,
  UpdateParticleAssignedToCellPredicate<PCParticle<LocalParticleContainer>>
      localParticlePredicate,
  UpdateParticlePairWithinCellPredicate<PCParticle<LocalParticleContainer>>
      particlePairPredicate
) {
  for (auto& localParticle : localParticles) {
    if ( localParticlePredicate(marker, *localParticle) ) {
      typename LocalParticleContainer::value_type pFirstActiveParticle = *activeParticles.begin();
      for (auto& chunkSize: numberOfActiveParticlesPerVertex) {
        for (int i=0; i<chunkSize; i++) {
          PCParticle<ActiveParticleContainer>&  activeParticle = *(pFirstActiveParticle+i);
          if ( particlePairPredicate(marker, *localParticle, activeParticle) ) {
            f(marker, *localParticle, activeParticle);

            if (tarch::la::oneGreater(activeParticle.getCellH(), localParticle->getCellH())) {
              tarch::multicore::Lock lock(internal::_multiscaleInteractionSemaphore);
              f(marker, activeParticle, *localParticle);
            }
          }
        }

        pFirstActiveParticle += chunkSize;
      }
    }
  }
}


template <typename ParticleContainer>
void swift2::kernels::coalesced::prefixcheck::forAllParticles(
    const peano4::datamanagement::VertexMarker&   marker,
    ParticleContainer&                            assignedParticles,
    int                                           numberOfAssignedParticles,
    ParticleUnaryOperatorOnVertex<typename ParticleContainer::DoFType>    auto f,
    UpdateParticleAssignedToVertexPredicate<typename ParticleContainer::DoFType> predicate
) {
  typename ParticleContainer::value_type pFirstParticle = *assignedParticles.begin();

  for (int i=0; i<numberOfAssignedParticles; i++) {
    PCParticle<ParticleContainer>&  particle = *(pFirstParticle+i);
    if (predicate(marker,particle)) {
      f(marker,particle);
    }
  }
}


template <typename ParticleContainer>
void swift2::kernels::coalesced::prefixcheck::forAllParticles(
  const peano4::datamanagement::CellMarker&                    marker,
  ParticleContainer&                                           localParticles,
  const std::vector<int>&                                      numberOfLocalParticles,
  ParticleUnaryOperatorOnCell<PCParticle<ParticleContainer>>   auto f,
  UpdateParticleAssignedToCellPredicate<PCParticle<ParticleContainer>>
            predicate
) {
  typename ParticleContainer::value_type pFirstParticle = *localParticles.begin();

  for (auto& chunkSize: numberOfLocalParticles) {
    for (int i=0; i<chunkSize; i++) {
      PCParticle<ParticleContainer>&  particle = *(pFirstParticle+i);
      if (predicate(marker,particle)) {
        f(marker,particle);
      }
    }
    pFirstParticle += chunkSize;
  }
}


/*
template <typename ParticleContainer>
void swift2::kernels::forAllParticlesVectoriseWithCheckPreamble(
  const peano4::datamanagement::VertexMarker&                  marker,
  ParticleContainer&                                           assignedParticles,
  int                                                          numberOfAssignedParticles,
  ParticleUnaryOperatorOnVertex<typename ParticleContainer::DoFType > auto f,
  UpdateParticleAssignedToVertexPredicate<typename ParticleContainer::DoFType> predicate
) {
  assertionEquals( assignedParticles.size(), numberOfAssignedParticles );

  typename ParticleContainer::value_type pFirstParticle = *assignedParticles.begin();

  constexpr int MaxVectorSize       = 8;

  int currentFirstParticleInChunk = 0;
  while (currentFirstParticleInChunk<numberOfAssignedParticles) {
    const int    currentChunkSize    = std::min(MaxVectorSize,numberOfAssignedParticles-currentFirstParticleInChunk);
    unsigned int evaluatedPredicates = 0;

    simtFor(particleInChunkNumber,currentChunkSize) {
      const bool evaluate    = predicate(marker,*(pFirstParticle+currentFirstParticleInChunk+particleInChunkNumber));
      int        evaluateBit = evaluate ? 1: 0;
      evaluatedPredicates   |= (evaluateBit << particleInChunkNumber);
    } endSimtFor

    auto update2 = [&]( unsigned int evaluatedPredicates, int indexOfFirstParticle) -> void {
      const int    currentChunkSize    = std::min(2,numberOfAssignedParticles-indexOfFirstParticle);
      if (currentChunkSize<=0) {
        // degenerated, return
      }
      else if (std::popcount(evaluatedPredicates)==2 and currentChunkSize==2) {
        simtFor(particleInChunkNumber,2) {
          f(marker,*(pFirstParticle+indexOfFirstParticle+particleInChunkNumber));
        } endSimtFor
      }
      else {
        if (indexOfFirstParticle & 0b01) {
          f(marker,*(pFirstParticle+indexOfFirstParticle+0));
        }
        if (indexOfFirstParticle & 0b10) {
          f(marker,*(pFirstParticle+indexOfFirstParticle+1));
        }
      }
    };

    auto update4 = [&]( unsigned int evaluatedPredicates, int indexOfFirstParticle) -> void {
      const int    currentChunkSize    = std::min(4,numberOfAssignedParticles-indexOfFirstParticle);
      if (currentChunkSize<=0) {
        // degenerated, return
      }
      else if (std::popcount(evaluatedPredicates)>=3 and currentChunkSize==4) {
        simtFor(particleInChunkNumber,4) {
          f(marker,*(pFirstParticle+indexOfFirstParticle+particleInChunkNumber));
        } endSimtFor
      }
      else {
        constexpr int LeftMask   = 0b0011;
        constexpr int RightMask  = 0b1100;
        int leftEvaluatedPredicates  = evaluatedPredicates & LeftMask;
        int rightEvaluatedPredicates = (evaluatedPredicates & RightMask) >> 2;
        update2( leftEvaluatedPredicates,  indexOfFirstParticle);
        update2( rightEvaluatedPredicates, indexOfFirstParticle+4);
      }
    };

    auto update8 = [&]( unsigned int evaluatedPredicates, int indexOfFirstParticle) -> void {
      const int    currentChunkSize    = std::min(8,numberOfAssignedParticles-indexOfFirstParticle);
      if (currentChunkSize<=0) {
        // degenerated, return
      }
      else if (std::popcount(evaluatedPredicates)>=6 and currentChunkSize==8) {
        simtFor(particleInChunkNumber,8) {
          f(marker,*(pFirstParticle+indexOfFirstParticle+particleInChunkNumber));
        } endSimtFor
      }
      else {
        constexpr int LeftMask   = 0b00001111;
        constexpr int RightMask  = 0b11110000;
        int leftEvaluatedPredicates  = evaluatedPredicates & LeftMask;
        int rightEvaluatedPredicates = (evaluatedPredicates & RightMask) >> 4;
        update4( leftEvaluatedPredicates,  indexOfFirstParticle);
        update4( rightEvaluatedPredicates, indexOfFirstParticle+4);
      }
    };

    update8( evaluatedPredicates, currentFirstParticleInChunk );

    currentFirstParticleInChunk += MaxVectorSize;
  }
}
*/


template <typename LocalParticleContainer, typename ActiveParticleContainer>
void swift2::kernels::coalesced::prefixcheck::forAllParticlePairs(
  const peano4::datamanagement::CellMarker&  marker,
  LocalParticleContainer&                    localParticles,
  ActiveParticleContainer&                   activeParticles,
  const std::vector<int>&                    numberOfLocalParticlesPerVertex,
  const std::vector<int>&                    numberOfActiveParticlesPerVertex,
  ParticleBinaryOperator<PCParticle<LocalParticleContainer>, PCParticle<ActiveParticleContainer>> auto f,
  UpdateParticleAssignedToCellPredicate<PCParticle<LocalParticleContainer>>
      localParticlePredicate,
  UpdateParticlePairWithinCellPredicate<PCParticle<LocalParticleContainer>>
      particlePairPredicate
) {
  for (auto& localParticle : localParticles) {
    if ( localParticlePredicate(marker, *localParticle) ) {
      typename LocalParticleContainer::value_type pFirstActiveParticle = *activeParticles.begin();

      for (auto& chunkSize: numberOfActiveParticlesPerVertex) {
        constexpr int SubChunkSize = 8;

        int particleIndex=0;
        while (particleIndex<chunkSize) {
          const int thisSubchunkSize = std::min(SubChunkSize,chunkSize-particleIndex);

          auto [updateParticleEffect, evaluateBottomUpParticleEffect] = swift2::kernels::internal::identifyInteractingParticles<SubChunkSize>(
            marker,
            localParticle,
            pFirstActiveParticle+particleIndex,
            thisSubchunkSize,
            particlePairPredicate
          );

          if (updateParticleEffect.all()) {
            for (int particleIndexWithinThisSubchunk=0; particleIndexWithinThisSubchunk<thisSubchunkSize; particleIndexWithinThisSubchunk++) {
              PCParticle<ActiveParticleContainer>&  activeParticle = *(pFirstActiveParticle+particleIndex+particleIndexWithinThisSubchunk);
              f(marker, *localParticle, activeParticle);
            }
          }
          else if (updateParticleEffect.any()) {
            for (int particleIndexWithinThisSubchunk=0; particleIndexWithinThisSubchunk<thisSubchunkSize; particleIndexWithinThisSubchunk++) {
              if (updateParticleEffect[particleIndexWithinThisSubchunk]) {
                PCParticle<ActiveParticleContainer>&  activeParticle = *(pFirstActiveParticle+particleIndex+particleIndexWithinThisSubchunk);
                f(marker, *localParticle, activeParticle);
              }
            }
          }

          if (evaluateBottomUpParticleEffect.any()) {
            for (int particleIndexWithinThisSubchunk=0; particleIndexWithinThisSubchunk<thisSubchunkSize; particleIndexWithinThisSubchunk++) {
              if (
                updateParticleEffect[particleIndexWithinThisSubchunk]
                and
                evaluateBottomUpParticleEffect[particleIndexWithinThisSubchunk]
              ) {
                tarch::multicore::Lock lock(internal::_multiscaleInteractionSemaphore);
                PCParticle<ActiveParticleContainer>&  activeParticle = *(pFirstActiveParticle+particleIndex+particleIndexWithinThisSubchunk);
                f(marker, activeParticle, *localParticle);
              }
            }
          }
          particleIndex+=SubChunkSize;
        }
      }
    }
  }
}
