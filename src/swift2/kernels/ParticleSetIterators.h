// This file is part of the SWIFT2 project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#pragma once


#include "ParticleUpdatePredicates.h"


#include "tarch/multicore/BooleanSemaphore.h"
#include "tarch/multicore/Lock.h"


namespace swift2 {
  namespace kernels {

    // template for convenience and code legibility.
    template <typename ParticleContainer>
    using PCParticle = typename std::remove_pointer<typename ParticleContainer::value_type>::type;

    /**
     * Definition of particle update (unary operation)
     *
     * The interface uses the marker, as it is the @ref swift_particle_new_solver "responsibility of any
     * update to check if the update is actually valid". That is, an
     * operator should always use a UpdateParticleAssignedToVertexPredicate
     * internally to check if it actually should update. How this knowledge
     * is taken into account is however up to the realisation. In the simplest
     * case, it is just a mere enclosing if statement.
     *
     * A typical operator looks like
     *
     * ~~~~~~~~~~~~~~~~~~~~~~~~
     * [=](
     *   const peano4::datamanagement::VertexMarker& marker,
     *   globaldata::MyParticle&                     particle
     * ) -> void {
     *   if ( ::swift2::kernels::localParticleCanBeMoved(marker, particle) ) {
     *     [...]
     *     particle.setMoveState(Particle::MoveState::Moved);
     *   }
     * }
     * ~~~~~~~~~~~~~~~~~~~~~~~~
     *
     * This realisation is stereotypical and also highlight one tiny detail:
     * If you alter the particle position, you have to reset the MoveState.
     * Other kernels do not have any additional bookkeeping.
     *
     * Swift offers a set of pre-defined timestepping and reduction kernels.
     * These are templates and hence have to be specialised:
     *
     * ~~~~~~~~~~~~~~~~~~~~~~~~
     * AlgorithmStep(
     *   [...]
     *   touch_vertex_first_time_kernel = """::swift2::kernels::forAllParticles( marker, assignedParticles, ::swift2::timestepping::resetMovedParticleMarker<globaldata::{}> );""".format( my_particle_type ),
     *   touch_vertex_last_time_kernel  = """::swift2::kernels::forAllParticles( marker, assignedParticles, ::swift2::timestepping::computeExplicitEulerWithGlobalTimeStepSize<globaldata::{}> );""".format( my_particle_type ),
     *   [...]
     * )
     * ~~~~~~~~~~~~~~~~~~~~~~~~
     *
     */
    template<typename F, typename Particle>
    concept ParticleUnaryOperatorOnVertex = requires (F f, const peano4::datamanagement::VertexMarker &marker, Particle &localParticle) {
        { f(marker, localParticle) } -> std::same_as<void>;
    };

    template<typename F, typename Particle>
    concept ParticleUnaryOperatorOnCell = requires (F f, const peano4::datamanagement::CellMarker &marker, Particle &localParticle) {
        { f(marker, localParticle) } -> std::same_as<void>;
    };

    /**
     * Definition of particle-particle interaction
     *
     * This definition works if LocalParticle is a real type, but it also
     * works if it is a pointer, as we then get references to pointers in the
     * signature which degenerates to the pointers in the source code.
     * However, I usually use it without any pointer semantics.
     *
     * The interface uses the marker, as it is the @ref swift_particle_new_solver "responsibility of any
     * update to check if the update is actually valid". That is, a binary
     * operator should always use a UpdateParticlePairWithinCellPredicate
     * internally to check if it actually should update. How this knowledge
     * is taken into account is however up to the realisation. In the simplest
     * case, it is just a mere enclosing if statement.
     *
     * So one we assume that we employ localParticleCanBeUpdatedInCellKernelFromAnyOtherParticle
     * as update rule, a function implementing the concept of
     * ParticleBinaryOperator resembles
     *
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * void myParticleBinaryOperator(
     *   const peano4::datamanagement::CellMarker& marker,
     *   globaldata::MyParticle&                   localParticle,
     *   globaldata::MyParticle&                   activeParticle
     * ) {
     *   if ( ::swift2::kernels::localParticleCanBeUpdatedInCellKernelFromAnyOtherParticle(
     *     marker,
     *     localParticle,
     *     activeParticle
     *   )) {
     *     [...] // your code
     *   }
     * }
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     * This function can now be used anywhere where a template expects a
     * ParticleBinaryOperator.
     *
     *
     * ## Using the function
     *
     * Once you have introduced your function (in a cpp file of your choice,
     * e.g.), you can hand it in via a plain myParticleBinaryOperator argument.
     *
     *
     * For very simple interactions that you define only once, don't want to
     * unit test, ... you might skip the explicit declaration and add the thing
     * in directly as a functor:
     *
     *
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * [=](
     *   const peano4::datamanagement::CellMarker& marker,
     *   globaldata::MyParticle&                   localParticle,
     *   globaldata::MyParticle&                   activeParticle
     * ) -> void {
     *   if ( ::swift2::kernels::localParticleCanBeUpdatedInCellKernelFromAnyOtherParticle(
     *     marker,
     *     localParticle,
     *     activeParticle
     *   )) {
     *     [...] // your code
     *   }
     * }
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     * ## Type-generic interaction operators
     *
     * Obviously, you might want to make this function
     * a template, so it can be used for different particles. It might also be
     * tempting to distinguish the type of active and local particles, so you
     * can have interactions between different species:
     *
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * template <typename LocalParticle, typename ActiveParticle>
     * void myParticleBinaryOperator(
     *   const peano4::datamanagement::CellMarker& marker,
     *   LocalParticle&                            localParticle,
     *   ActiveParticle&                           activeParticle
     * ) {
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     * The only thing to take into account is that the compiler now might
     * struggle to deduce the type. If this happens, simply hand in
     *
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * myParticleBinaryOperator<globaldata::MyType,globaldata::MyType>
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     * as argument where myParticleBinaryOperator had been sufficient before.
     */
    template<typename F, typename LocalParticle, typename ActiveParticle>
    concept ParticleBinaryOperator = requires (F f, const peano4::datamanagement::CellMarker& marker, LocalParticle& localParticle, ActiveParticle& activeParticle) {
        { f(marker, localParticle, activeParticle) } -> std::same_as<void>;
    };

    /**
     * Run over all particles and update them independent of each other
     *
     * The routine accepts a container over particle pointers, but the
     * functor f actually accepts references. It is the responsibility of
     * this routine to map pointers onto references.
     *
     * ## Predicate
     *
     * The predicate can be used to mask out certain updates.
     *
     * We distinguish two use cases for the particle self-interactions:
     *
     * 1. We want to execute a stand-alone computation over the particle, e.g.
     *    the kick step, which is does not require any particle-particle
     *    contribution.
     * 2. We want to execute computations after and before the particle-particle
     *    interactions kernels, e.g. to 'prepare' or 'end' a given calculation in
     *    SPH loops (e.g. density).
     *
     * The most popular predicates therefore are:
     *
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * ::swift2::kernels::localParticleCanBeUpdatedInVertexKernel<typename ParticleContainer::DoFType>
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     * and
     *
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * ::swift2::kernels::alwaysUpdateInVertexKernel<typename ParticleContainer::DoFType>
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     *
     * ## Consistency remarks
     *
     * In order to ensure the self-interaction kernels execute consistently
     * during the mesh traversals for these type of operations, the user should
     * bear in mind the difference between these two cases:
     *
     *  - The 'prepare' case should be mapped into touchVertexFirstTime(). Peano
     *    internally ensures that CellHasUpdatedParticle is false in this situation.
     *  - The 'end' case should be mapped into touchVertexLastTime. Peano
     *    internally ensures that CellHasUpdatedParticle is true in this situation.
     *
     * This routine does not vectorise over the particles. If any vectorisation
     * is used, you'll see the vector instructions arise from the actual compute
     * kernel.
     *
     * @param ParticleContainer A subtype of std::list<Particle *>
     */
    template <typename ParticleContainer>
    void forAllParticles(
      const peano4::datamanagement::VertexMarker&                         marker,
      ParticleContainer&                                                  assignedParticles,
      int                                                                 numberOfCoalescedAssignedParticles,
      ParticleUnaryOperatorOnVertex<typename ParticleContainer::DoFType > auto f,
      UpdateParticleAssignedToVertexPredicate<typename ParticleContainer::DoFType> predicate
    );


    /**
     * Loop over all particles within a cell
     *
     * ## Predicates
     *
     * The most popular predicates in use are
     *
     * ~~~~~~~~~~~~~~~~~~~~~~~~
     * ::swift2::kernels::alwaysUpdateInCellKernel<typename ParticleContainer::DoFType>
     * ~~~~~~~~~~~~~~~~~~~~~~~~
     *
     * and
     *
     * ~~~~~~~~~~~~~~~~~~~~~~~~
     * ::swift2::kernels::localParticleCanBeUpdatedInCellKernel<PCParticle<ParticleContainer>>
     * ~~~~~~~~~~~~~~~~~~~~~~~~
     */
    template <typename ParticleContainer>
    void forAllParticles(
      const peano4::datamanagement::CellMarker&                  marker,
      ParticleContainer&                                         localParticles,
      const std::vector<int>&                                    numberOfCoalescedLocalParticlesPerVertex,
      ParticleUnaryOperatorOnCell<PCParticle<ParticleContainer>> auto f,
      UpdateParticleAssignedToCellPredicate<PCParticle<ParticleContainer>>   predicate
    );


    /**
     * Run over all local particle-active particle combinations
     *
     * The generic implementation realises a nested loops: We call interaction
     * per pair of local and active particles. The interaction functor may
     * modify the local particle. It is the responsibility of the update
     * predicate to ensure that no particle is updated twice, even if it is
     * sitting right at the face in-between two cell. The predicate also
     * should check if there's a self-interaction and mask it out if
     * appropriate.
     *
     * The routine accepts a container over particle pointers, but the
     * functor f actually accepts references. It is the responsibility of
     * this routine to map pointers onto references. We use
     *
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *   for (auto& localParticle : localParticles) {
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     * here, but it is important to note that this reference still becomes a
     * reference to a pointer as localParticle is a container over pointers.
     *
     * In Swift's terminology, this is a non-symmetric XXX calculation where XXX
     * is the compute kernel (such as density):
     * Our implementations loops over all local particles (outer loop) and, per
     * local particle then over all active particles (inner loop). Inside this
     * second loop, we compute a force acting from the active particle
     * onto the local particle.
     *
     * To avoid self-interaction, users have to ensure that @f$ f(p,p) :=0 @f$: If a
     * particle acts on itself, the arising force equals zero. This should
     * always be the case, as the predicate passed in here is an optimisation,
     * i.e. users should not rely on it to mask out self-interaction a priori.
     *
     * Equally important is a second case distinction: If a local and an active
     * particle reside on the same level, we know that the symmetric force
     * component will be computed later on. Let L be the local one and A the
     * active one, we have
     *
     * @f$ f(L,A) = -f(A,L) @f$
     *
     * but this antisymmetry is not exploited in this kernel. We know that
     * there will be another loop iteration or cell interaction kernel call
     * which takes care of the negative complementary force.
     *
     * However, if A resides on a coarser level than L, this assumption is wrong.
     * We need to add the force explicitly as discussed in @ref page_toolbox_particles_mesh_traversal.
     *
     * ## Multiscale interactions
     *
     * This routine follows Peano's @ref page_toolbox_particles_mesh_traversal "generic multiscale discussion",
     * where we point out that we have correctly take particles into account
     * which reside on coarser levels. This is necessary if some particles have
     * larger cut-off radii than the others or if we work with adaptive meshes.
     *
     *
     * ## Predicates
     *
     * This would be a generic predicate as most kernels require it:
     *
     * ~~~~~~~~~~~~~~~~~~~~~~~
     *       UpdateParticleAssignedToCellPredicate<PCParticle<LocalParticleContainer>>
     *                 localParticlePredicate = ::swift2::kernels::alwaysUpdateInCellKernel<PCParticle<LocalParticleContainer>>,
     *       UpdateParticlePairWithinCellPredicate<PCParticle<LocalParticleContainer>>
     *                 particlePairPredicate = ::swift2::kernels::localParticleCanBeUpdatedInCellKernelFromAnyOtherParticle<PCParticle<LocalParticleContainer>>
     * ~~~~~~~~~~~~~~~~~~~~~~~
     *
     * However, you can use a more bespoke version as follows:
     *
     * ~~~~~~~~~~~~~~~~~~~~~~~
     *       UpdateParticleAssignedToCellPredicate<PCParticle<LocalParticleContainer>>
     *                 localParticlePredicate = ::swift2::kernels::localParticleCanBeUpdatedInCellKernel<PCParticle<LocalParticleContainer>>,
     *       UpdateParticlePairWithinCellPredicate<PCParticle<LocalParticleContainer>>
     *                 particlePairPredicate = ::swift2::kernels::localParticleCanBeUpdatedInCellKernelFromAnyOtherParticle<PCParticle<LocalParticleContainer>>
     * ~~~~~~~~~~~~~~~~~~~~~~~
     *
     *
     * ### Hydro Force calculation particle-particle kernel
     *
     * @todo This documentation is likely outdated
     *
     * Non-symmetric force interaction between two particles (as per
     * Swift terminology). In this, only the Local particle is updated (in the
     * symmetric case, both are updated at the same time). Time derivative of
     * the internal energy, du/dt, is also updated here. Subscripts a and b
     * represent indices of Local and Active particles, respectively:
     *
     * h_a: Smoothing length of Local particle
     * h_b: Smoothign length of Active particle
     * dx : Vector separating the two particles (x_loc - x_act)
     * r  : Distance between the two particles  (|| dx ||)
     *
     * To inject the hydro force, we typically use the code snippet similar to
     *
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * cell_kernel_for_force_calculation = "::swift2::forAllParticlePairs(marker, localParticles, activeParticles,::swift2::kernels::forceKernel<globaldata::HydroPart>);"
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     * As we have to know the particle type for the injected functor, the SPH
     * particle for example replaces this similar to
     *
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * cell_kernel_for_force_calculation = "::swift2::forAllParticlePairs(marker, localParticles, activeParticles,::swift2::kernels::forceKernel<globaldata::{}>);".format(self.name)
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     * @param LocalParticleContainer  A subtype of std::list<Particle *>
     * @param ActiveParticleContainer A subtype of std::list<Particle *>
     */
    template <typename LocalParticleContainer, typename ActiveParticleContainer>
    void forAllParticlePairs(
      const peano4::datamanagement::CellMarker&  marker,
      LocalParticleContainer&                    localParticles,
      ActiveParticleContainer&                   activeParticles,
      const std::vector<int>&                    numberOfCoalescedLocalParticlesPerVertex,
      const std::vector<int>&                    numberOfCoalescedActiveParticlesPerVertex,
      ParticleBinaryOperator<PCParticle<LocalParticleContainer>, PCParticle<ActiveParticleContainer>> auto f,
      UpdateParticleAssignedToCellPredicate<PCParticle<LocalParticleContainer>>
          localParticlePredicate = ::swift2::kernels::localParticleCanBeUpdatedInCellKernel<PCParticle<LocalParticleContainer>>,
      UpdateParticlePairWithinCellPredicate<PCParticle<LocalParticleContainer>>
          particlePairPredicate = ::swift2::kernels::localParticleCanBeUpdatedInCellKernelFromAnyOtherParticle<PCParticle<LocalParticleContainer>>
    );

    namespace internal {
      /**
       * If particles on multiple levels interact, we have to be very
       * careful and ensure that multiple subcells (if mapped onto tasks)
       * do not write to the same coarse grid particles concurrently.
       */
      extern tarch::multicore::BooleanSemaphore  _multiscaleInteractionSemaphore;


      /**
       * Identify interacting particles
       *
       * In several situations, we have a continuous chunk of particles and
       * want to identify which of these interact with each other due to the
       * particlePairPredicate. Besides a boolset that identifies those that
       * do interact, we also return a bitfield which identifies where we would
       * have to identify multiscale interactions.
       */
      template <int SubChunkSize, typename LocalParticle, typename ActiveParticle>
      std::pair< std::bitset<SubChunkSize>, std::bitset<SubChunkSize> > identifyInteractingParticles(
          const peano4::datamanagement::CellMarker&  marker,
          LocalParticle*                             localParticle,
          ActiveParticle*                            firstActiveParticle,
          int                                        thisSubchunkSize,
          UpdateParticlePairWithinCellPredicate<LocalParticle>  particlePairPredicate
      );
    }
  } // namespace kernels
} // namespace swift2


#include "ParticleSetIterators.cpph"


// Include the specialisations. Has to be last thing here, as the
// specialisations will use our typedefs.
#include "swift2/kernels/cppiterators/ParticleSetIterators.h"
#include "swift2/kernels/ompiterators/ParticleSetIterators.h"
