#include "swift2/kernels/ParticleUpdatePredicates.h"
#include "peano4/utils/Loop.h"


#include <bit>
#include <list>


template <typename ParticleContainer>
void swift2::kernels::forAllParticles(
    const peano4::datamanagement::VertexMarker&   marker,
    ParticleContainer&                            assignedParticles,
    int                                           numberOfCoalescedAssignedParticles,
    ParticleUnaryOperatorOnVertex<typename ParticleContainer::DoFType>    auto f,
    UpdateParticleAssignedToVertexPredicate<typename ParticleContainer::DoFType> predicate
) {
  for (auto& particle : assignedParticles) {
    if (predicate(marker,*particle)) {
      f(marker,*particle);
    }
  }
}


template <typename ParticleContainer>
void swift2::kernels::forAllParticles(
  const peano4::datamanagement::CellMarker&                    marker,
  ParticleContainer&                                           localParticles,
  const std::vector<int>&                                      numberOfCoalescedLocalParticles,
  ParticleUnaryOperatorOnCell<PCParticle<ParticleContainer>>   auto f,
  UpdateParticleAssignedToCellPredicate<PCParticle<ParticleContainer>>
            predicate
) {
  for (auto& particle : localParticles) {
    if (predicate(marker,*particle)) {
      f(marker,*particle);
    }
  }
}


template <typename LocalParticleContainer, typename ActiveParticleContainer>
void swift2::kernels::forAllParticlePairs(
  const peano4::datamanagement::CellMarker&  marker,
  LocalParticleContainer&                    localParticles,
  ActiveParticleContainer&                   activeParticles,
  const std::vector<int>&                    numberOfCoalescedLocalParticlesPerVertex,
  const std::vector<int>&                    numberOfCoalescedActiveParticlesPerVertex,
  ParticleBinaryOperator<PCParticle<LocalParticleContainer>, PCParticle<ActiveParticleContainer>> auto f,
  UpdateParticleAssignedToCellPredicate<PCParticle<LocalParticleContainer>>
      localParticlePredicate,
  UpdateParticlePairWithinCellPredicate<PCParticle<LocalParticleContainer>>
      particlePairPredicate
) {
  for (auto& localParticle : localParticles) {
    if ( localParticlePredicate(marker, *localParticle) ) {
      for (auto& activeParticle : activeParticles) {
        if ( particlePairPredicate(marker, *localParticle, *activeParticle) ) {
          f(marker, *localParticle, *activeParticle);
          if (tarch::la::oneGreater(activeParticle->getCellH(), localParticle->getCellH())) {
            tarch::multicore::Lock lock(internal::_multiscaleInteractionSemaphore);
            f(marker, *activeParticle, *localParticle);
          }
        }
      }
    }
  }
}


template <int SubChunkSize, typename LocalParticle, typename ActiveParticle>
std::pair< std::bitset<SubChunkSize>, std::bitset<SubChunkSize> >  swift2::kernels::internal::identifyInteractingParticles(
    const peano4::datamanagement::CellMarker&  marker,
    LocalParticle*                             localParticle,
    ActiveParticle*                            firstActiveParticle,
    int                                        thisSubchunkSize,
    UpdateParticlePairWithinCellPredicate<LocalParticle>   particlePairPredicate
) {
  std::bitset<SubChunkSize> updateParticleEffect(0);
  std::bitset<SubChunkSize> evaluateBottomUpParticleEffect(0);

  for (int particleIndexWithinThisSubchunk=0; particleIndexWithinThisSubchunk<thisSubchunkSize; particleIndexWithinThisSubchunk++) {
    ActiveParticle*  activeParticle = firstActiveParticle+particleIndexWithinThisSubchunk;

    updateParticleEffect[particleIndexWithinThisSubchunk]           = particlePairPredicate(marker, *localParticle, *activeParticle);
    evaluateBottomUpParticleEffect[particleIndexWithinThisSubchunk] = tarch::la::oneGreater(activeParticle->getCellH(), localParticle->getCellH());
  }

  return { updateParticleEffect, evaluateBottomUpParticleEffect };
}
