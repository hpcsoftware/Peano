#include "hydro_dimensions.h"
#include "kernel_hydro.h"

template <typename Particle>
void swift2::kernels::legacy::first_init_particle(Particle* particle) {

  // macro expansions from swift2/legacy/kernels are cumbersome otherwise.
  using namespace swift2::kernels::legacy::kernelHydro;

  particle->setUDot(0.0);
  particle->setHDot(0.0);
  particle->setF(0.0);
  particle->setA(0.0);

  particle->setHasNoNeighbours(false);
  particle->setSmoothingLengthIterCount(0);
  particle->setSmoothingLengthConverged(false);

  // Artificial viscosity scheme
  particle->setV_sig_AV(2.0 * particle->getSoundSpeed());
  particle->setBalsara(0.0);

  // All the inits done each step too.
  hydro_init_particle(particle);

  // Verify smoothing length and search radius.
  double h = particle->getSmoothingLength();

  if (h == 0.) {
    // Make an initial guess for the smoothing length.
    // If the value for smoothing length is non-zero, assume that a
    // reasonable guess has already been made, and e.g. has been
    // provided by the ICs. Otherwise, assume that the search radius
    // is reasonable.
    h = particle->getSearchRadius() * 0.5 * kernel_gamma_inv;
  }

  h = std::min(h, Particle::getSmlMax());
  h = std::max(h, Particle::getSmlMin());

  // the smoothing length now must be somewhere between the
  // user provided min and max. If that doesn't fit within
  // the search radius, then the search radius is chosen poorly
  // and needs to be adapted.

  if (h * kernel_gamma > particle->getSearchRadius()) {
    particle->setSearchRadius(h * kernel_gamma);
  }

  particle->setSmoothingLength(h);

  // Set left-right bounds
  particle->setSmlIterLeftBound(Particle::getSmlMin());
  particle->setSmlIterRightBound(Particle::getSmlMax());
}


template <typename Particle>
void swift2::kernels::legacy::hydro_init_particle(Particle* particle) {

  if (particle->getSmoothingLengthConverged()) {
    return;
  }

  particle->setWcount(0.0);
  particle->setWcount_dh(0.0);

  particle->setDensity(0.0);
  particle->setRho_dh(0.0);

  particle->setDiv_v(0.0);
  particle->setRot_v(0.0);

  particle->setHasNoNeighbours(false);
#if PeanoDebug > 0
  particle->setDensityNeighbourCount(0);
#endif
}
