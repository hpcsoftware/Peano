// This file is part of the Peano project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#pragma once


#include "tarch/la/DynamicMatrix.h"

namespace tarch {
  namespace la {
  
    DynamicMatrix transpose(const DynamicMatrix& matrix);
  
  }
}
