#ifdef UseOpenblas
#include <cblas.h>
#endif

template <int Rows, int Cols, typename Scalar>
tarch::la::Vector<Rows, Scalar> tarch::la::multiply(
  const Matrix<Rows, Cols, Scalar>& matrix,
  const Vector<Cols, Scalar>&       vector
) {
  tarch::la::Vector<Rows, Scalar> result(Scalar(0));
  for (int i = 0; i < Rows; i++) {
    for (int j = 0; j < Cols; j++) {
      result(i) += matrix(i, j) * vector(j);
    }
  }
  return result;
}


template <int Rows, int Cols, typename Scalar>
tarch::la::Vector<Rows, Scalar> tarch::la::operator*(
  const Matrix<Rows, Cols, Scalar>& matrix,
  const Vector<Cols, Scalar>&       vector
) {
  return multiply(matrix, vector);
}

#ifdef UseOpenblas
template <int Rows, int Cols>
tarch::la::Vector<Rows, double> tarch::la::operator*(
  const Matrix<Rows, Cols, double>& matrix,
  const Vector<Cols, double>&       vector
) {
  tarch::la::Vector<Rows, double> output;
  cblas_dgemv(
    CblasRowMajor,
    CblasNoTrans,
    Rows,
    Cols,
    1,
    matrix.data(),
    Rows,
    vector.data(),
    1,
    0,
    output.data(),
    1
  );
  return output;
}
#endif

template <int Size, typename Scalar>
tarch::la::Matrix<Size, Size, Scalar> tarch::la::outerDot(
  const Vector<Size, Scalar>& lhs,
  const Vector<Size, Scalar>& rhs
) {
  tarch::la::Matrix<Size, Size, Scalar> result;
  for (int i = 0; i < Size; i++) {
    for (int j = 0; j < Size; j++) {
      result(j, i) = lhs(j) * rhs(i);
    }
  }
  return result;
}
