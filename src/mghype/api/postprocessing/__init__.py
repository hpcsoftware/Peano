""" This file is part of the Peano multigrid project. For conditions of 
 distribution and use, please see the copyright notice at www.peano-framework.org
""" 
from .Parser import parse_residuals
from .Parser import parse_solution_updates