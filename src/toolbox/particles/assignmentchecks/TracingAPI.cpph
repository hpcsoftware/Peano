// This file is part of the Peano project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org

#pragma once

#include "Database.h"

template <typename ParticleContainer>
std::vector<toolbox::particles::assignmentchecks::ParticlePosition> toolbox::
  particles::assignmentchecks::recordParticlePositions(
    const ParticleContainer& container
  ) {

  std::vector<toolbox::particles::assignmentchecks::ParticlePosition> result(
    container.size()
  );

#ifdef AssignmentChecks
  int entry = 0;
  for (auto& p : container) {
    result[entry] = p->getX();
    entry++;
  }
#endif

  return result;
}


template <typename ParticleContainer>
void toolbox::particles::assignmentchecks::traceParticleMovements(
  const ParticleContainer&                     container,
  const std::vector<ParticlePosition>&         recordedPositions,
  const tarch::la::Vector<Dimensions, double>& vertexX,
  const tarch::la::Vector<Dimensions, double>& vertexH,
  int                                          spacetreeId
) {

#ifdef AssignmentChecks

  assertionEquals(container.size(), recordedPositions.size());

  typename ParticleContainer::const_iterator    pParticle = container.begin();
  std::vector<ParticlePosition>::const_iterator pRecordedPosition
    = recordedPositions.begin();

  using Particle = typename std::remove_pointer<
    typename ParticleContainer::value_type>::type;

  while (pParticle != container.end()) {

    if ((*pParticle)->getParallelState() == Particle::ParallelState::Local) {
      moveParticle(
        pruneTypeName<Particle>(),
        *pRecordedPosition,
        (*pParticle)->getX(),
        (*pParticle)->getPartid(),
        vertexX,
        vertexH,
        spacetreeId,
        "toolbox::particles::assignmentchecks::traceParticleMovements()"
      );
    }

    pParticle++;
    pRecordedPosition++;
  }
#endif
}
