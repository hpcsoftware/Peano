#pragma once

#define DT_DRIFT ((float) 1e-6)
#define DT_KICK_HYDRO ((float) 5e-7)
#define N_PART 8000
#define STEPS 10
#define LEVEL 7
#define STEPS_BETWEEN_DUMPS 0
