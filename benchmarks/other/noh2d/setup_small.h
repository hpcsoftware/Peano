#pragma once

#define DT_DRIFT ((float) 1e-4)
#define DT_KICK_HYDRO ((float) 5e-5)
#define N_PART 100
#define STEPS 1000
#define LEVEL 5
#define STEPS_BETWEEN_DUMPS 10
