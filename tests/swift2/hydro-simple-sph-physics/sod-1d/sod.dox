/**

 @page tests_swift2_hydro_simple_sph_sod_1d Sod Shockwave 1d (Simple SPH)

The setup runs the 1d sod shockwave with a simple SPH scheme, i.e. fixed
interaction radius. By default, we embed the 1d setup into a 2d mesh, but
you can also change it to three dimensions.

## Prepare the run

First of all, we have to create the initial conditions.


        python3 makeIC.py

This one should give you a file ```sodShock.hdf5```. If you encounter
issues around HDF5, it makes sense to consult our general discussion of
@ref swift_initial_conditions "Swift's initial conditions".


## Create the setup

The setup can be defined via one simple Python call


        python3 sod.py


Passing in --help gives you all the instructions needed. This documentation
hosts a script (below) which you can alternatively copy n paste into your
terminal. Alternately, look into the contents of `run.sh`. (Or just run `run.sh`)


## Postprocessing

use the script
        python3 plotSolution.py

to generate the output plot. It should look something like this:

  @image html SodShockSolution.png


 */


