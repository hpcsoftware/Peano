{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "b20b6371-d5ef-49c7-af22-7398bead6492",
   "metadata": {},
   "source": [
    "# The acoustic equations - a vanilla ExaHyPE 2 solver\n",
    "\n",
    "This is the first part of the ExaHyPE 2 tutorials. This part of the tutorial will briefly go over a few things:\n",
    "\n",
    "- how to import the Peano 4 and ExaHyPE 2 API in Python (which is a repetition of the hitchhiker's guide);\n",
    "- the structure of an ExaHyPE 2 script;\n",
    "- how to define a PDE solver in ExaHyPE 2;\n",
    "- running and visualizing the results of an ExaHyPE 2 run.\n",
    "\n",
    "Before we do this however, we will quickly define the PDE that is to come. As the name of the file will have already indicated,\n",
    "this is the two-dimensional acoustic equation which appears as follows:\n",
    "\n",
    "\\begin{equation*}\n",
    "    \\frac{\\partial}{\\partial t}\\left(\n",
    "    \\begin{array}{lr} p \\\\\n",
    "                      v_1 \\\\\n",
    "                      v_2\n",
    "                      \\end{array} \\right) +\n",
    "    \\nabla \\begin{pmatrix}\n",
    "                      K_0 v_1          & K_0 v_2\\\\\n",
    "                      \\frac{p}{\\rho}   & 0 \\\\\n",
    "                      0                & \\frac{p}{\\rho}\n",
    "    \\end{pmatrix}  = \\vec{0}\n",
    "\\end{equation*}\n",
    "\n",
    "where $p$ is the pressure, and $v_1$ and $v_2$ are the velocity respectively in the $x-$ and $y-$direction.\n",
    "It additionally uses two parameters which, for simplicity's sake we have assumed to remain constant over the domain.\n",
    "These are the density $\\rho=1$ and the bulk modulus $K_0=4$, which you can think of as the squishiness of the material. To implement this equation, we need two things: a flux and a maximal eigenvalue. The maximal eigenvalue is the greatest eigenvalue of the flux tensor, which is:\n",
    "\n",
    "\\begin{equation*}\n",
    "    \\lambda = \\sqrt{\\frac{K_0}{\\rho}}\n",
    "\\end{equation*}\n",
    "\n",
    "\n",
    "## Configuring the environment\n",
    "\n",
    "The first thing we will do is importing the Peano 4 and ExaHyPE 2 API. While we could do this in the terminal via `export PYTHONPATH=$PYTHONPATH:$PEANO_ROOT/python`, We again work directly within the notebook. Once on the Python path, the core packages can be imported just like any other package:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "42b3773f-814e-4a0a-b4d9-70f9914238ac",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys, os\n",
    "\n",
    "sys.path.insert(0, os.path.abspath(\"../../../../python/\"))\n",
    "\n",
    "import peano4, exahype2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79b69221-d58d-4065-80d7-b40d502565da",
   "metadata": {},
   "source": [
    "If some of these imports fail due to missing packages, either install through your preferred system environment or install them straightaway fom within the notebook (this is not the elegant version, as you might spot/derive from the --break-system-packages attribute):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "67f83ef9-40da-47b5-b1f4-f6c2a7d4e3b9",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "!{sys.executable} -m pip install --break-system-packages scipy mpmath sympy"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "60157557-94c0-4023-afb1-a0259efcea39",
   "metadata": {},
   "source": [
    "## A first solver\n",
    "\n",
    "In the hitchhikers guide (@ref tutorials_exahype2_hitchhikers-guide) we created an empty project. This time, we take this project and add an actual solver. We pick a plain Finite Volume solver. The tutorials comprise a very hands-on description of this numerical scheme (@ref page_tutorials_exahype2_numerics_finite_volumes) but actually we don't need to know any of these \"details\" at this point. The parameters of the solver should be self-explaining. Alternatively, you can consult the source code documentation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4693a991-d20e-4d74-a42a-ab636c45dec4",
   "metadata": {},
   "outputs": [],
   "source": [
    "exahype2_project = exahype2.Project(\n",
    "    namespace=[\"tutorials\", \"exahype2\", \"exercises\", \"acoustic\"],\n",
    "    project_name=\"Acoustic\",\n",
    "    directory=\".\",\n",
    "    executable=\"exahype2-acoustic\"\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cec7e8b1-fcf4-4a4b-bbbe-db4c1353e900",
   "metadata": {},
   "outputs": [],
   "source": [
    "dimensions = 2\n",
    "patch_size = 16\n",
    "cell_h = 1.1 * 2.0 / (3.0 ** 3)\n",
    "\n",
    "my_solver = exahype2.solvers.fv.rusanov.GlobalFixedTimeStep(\n",
    "    name                  = \"PlanarAcousticSolver\",\n",
    "    patch_size            = patch_size,\n",
    "    min_volume_h          = cell_h/patch_size,\n",
    "    max_volume_h          = cell_h/patch_size,\n",
    "    normalised_time_step_size = 0.01,\n",
    "    unknowns              = 3,\n",
    "    auxiliary_variables   = 0\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ad72106f-9c43-4402-bead-5da64d8b6e62",
   "metadata": {},
   "source": [
    "Maybe the one thing worth mentioning is the distinction between unknowns and auxiliary variables: Only unknowns evolve with the PDE. The auxiliary parameters are not subject to flux, ncp, sources, and so forth. They can hold constants such as material parameters or mesh distortion vectors, but also helper variables that you update with some other logic.\n",
    "\n",
    "\n",
    "With our solver defined, we can let it know what our equation will look like. Notably, we can define which PDE terms we wish to have and what they look like. In this case, we will let it know that it only need a classical flux.\n",
    "`exahype2.solvers.PDETerms.User_Defined_Implementation` which lets the project know that we will manually define the flux in an implementation file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4f39fe1c-efb6-4deb-8201-623ac356a5c8",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_solver.set_implementation(\n",
    "    flux = exahype2.solvers.PDETerms.User_Defined_Implementation\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5d103f86-d616-44f8-8d33-94fd3d76be28",
   "metadata": {},
   "source": [
    "## Configuring the ExaHyPE 2 project:\n",
    "\n",
    "We now add our solver to the project and set an output path for our results. We create it first through the notebook just to be sure that everything is in place:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "28470077-4b14-487f-abb8-895737bd01a0",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir solutions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d46e96ca-f16b-4e41-8fc6-a84845b14b0d",
   "metadata": {},
   "outputs": [],
   "source": [
    "exahype2_project.add_solver(my_solver)\n",
    "exahype2_project.set_output_path(\"solutions\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eed7f6bb-002e-416b-9a68-2588089b510b",
   "metadata": {},
   "source": [
    "Now that we have a well-rounded ExaHyPE 2 project, let's populate it with some information:\n",
    "This is essentially the domain our solver will run in.\n",
    "The project needs to know the domain size, how long the simulation should run,\n",
    "how often it should plot the solution, and what type of boundary conditions we want to see."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "965f6c49-dc40-4798-a067-a7caa225d937",
   "metadata": {},
   "outputs": [],
   "source": [
    "exahype2_project.set_global_simulation_parameters(\n",
    "    dimensions            = 2,\n",
    "    size                  = [2.0,  2.0],\n",
    "    offset                = [-1.0, -1.0],\n",
    "    min_end_time          = 1.414,  # sqrt(2)\n",
    "    max_end_time          = 1.414,  # sqrt(2)\n",
    "    first_plot_time_stamp = 0.0,\n",
    "    time_in_between_plots = 0.1,\n",
    "    periodic_BC           = [True, True],\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3af78a79-4aca-4bda-a65c-1f3666c53bbc",
   "metadata": {},
   "source": [
    "Now we kick the lowering off:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0ed3c71c-b212-41b9-8aef-d220e6c2c828",
   "metadata": {},
   "outputs": [],
   "source": [
    "exahype2_project.set_Peano4_installation(\"../../../../\", peano4.output.CompileMode.Release)\n",
    "peano4_project = exahype2_project.generate_Peano4_project()\n",
    "peano4_project.generate()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6efbfc8b-79b8-483e-9680-f9519adf2f20",
   "metadata": {},
   "source": [
    "## Adding the actual Physics\n",
    "\n",
    "So far, we have created a framework for a solver, but we have not written anything actually. We only told ExaHyPE that there will b a user-defined soler one day. So let's see what we actually got from the code so far:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "03449f2b-589b-4cb7-aaeb-14b5b471a567",
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls *.cpp *.h"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4b10b4c1-d3b8-47bc-84e1-9f47c802eda6",
   "metadata": {},
   "source": [
    "ExaHyPE/Peano have given us a couple of files:\n",
    "- There is the main file plus its header. This is kind of the driver of our simulation. We can alter it, but often the default does the job.\n",
    "- There is the Constants.h which holds all the global simulation parameter that we've specified before.\n",
    "- The AbstractPlanarAcousticSolver class is an implementation of our numerical scheme of choice and the baseline of our actual solver.\n",
    "- Its implementation PlanarAcousticSolver then holds the actual physics.\n",
    "Let's have a look into this one:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d87f5f9a-3cd7-4146-931a-4213c39d7f35",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat PlanarAcousticSolver.h"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5d4be0ce-ff8e-4baa-9fb8-dc26fcd4541e",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat PlanarAcousticSolver.cpp"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "66dc2a19-e4e2-49fe-b834-9d8fb365a4ea",
   "metadata": {},
   "source": [
    "You can obviously study the superclass, as well. But for our purpose the only important thing is that are four functions that we have to befill. Actually, we can ignore the boundary implementation, as we have picked periodic BCs, i.e. they won't be called anyway. This leaves us with the flux and the eigenvalue and the initial condition.\n",
    "\n",
    "Our development workflow from hereon is clear:\n",
    "\n",
    "1. We write those functions.\n",
    "2. We build the code (that's a plain `make` as Peano gives us a good old Makefile)\n",
    "3. We run the simulation\n",
    "\n",
    "## Exercise: Implement the flux and eigenvalue\n",
    "\n",
    "Open the cpp file of your implementation and add in the realisation of the flux and the eigenvalue. The formulae can be found above. The eigenvalue is always the same independent of any parameters and returns a scalar as per function signature. The flux is either first column or the second column from the matrix above, depending on the value of normal. For the initial condition, let's use a very simple hard-coded one for the time being:\n"
   ]
  },
  {
   "cell_type": "raw",
   "id": "c5ff7e5f-cb82-49c1-bda0-27efd385ffdb",
   "metadata": {},
   "source": [
    "     void ::exahype2::...::initialCondition(\n",
    "       [[maybe_unused]] const double* __restrict__                   Q, // Q[3+0]\n",
    "       ...\n",
    "     ) {\n",
    "       const double val = cos(-tarch::la::PI*(volumeCentre[0] + volumeCentre[1]));\n",
    "       const double K0 = 4.0, rho = 1.0;\n",
    "       const double kr = std::sqrt(2 * K0 * rho);\n",
    "       Q[1] = val;\n",
    "       Q[2] = val;\n",
    "       Q[0] = kr * val;  // that's the p\n",
    "     }\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3bf559f6-0811-4f0e-98c3-7d31ee914ac2",
   "metadata": {},
   "source": [
    "It might be more elegant to move rho and K0 as static consts into the header of the solver, but this does the job. Next, compile and execute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9354d65c-be51-4d7d-99d5-5b687790ac63",
   "metadata": {},
   "outputs": [],
   "source": [
    "!make -j"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1e254dc2-9dfd-466c-955a-6c01a138b96d",
   "metadata": {},
   "outputs": [],
   "source": [
    "!./exahype2-acoustic"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d19285ef-73c1-4924-89bf-940ad50edb2d",
   "metadata": {},
   "source": [
    "## Visualisation\n",
    "\n",
    "Finally, once the program has finished running, we can convert the output from the native peano4 format into something that can be visualised.\n",
    "\n",
    "Peano 4 provides a script for this, which can be called via `python3 $PEANO_ROOT/python/peano4/visualisation/render.py` followed by the name of the files we want to convert.\n",
    "\n",
    "This relies on the **ParaView/VTK** Python libraries, and converts the files into .vtu files, which can be visualised for example using ParaView."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e96c7b99-b899-43df-aaf4-f981ba59f509",
   "metadata": {},
   "outputs": [],
   "source": [
    "import subprocess\n",
    "subprocess.call(\n",
    "    [\"python3\", \"../../../../python/peano4/visualisation/render.py\", \"solutions/solution-PlanarAcousticSolver.peano-patch-file\"]\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e379f8bb-edb6-40e4-a614-3ae1099f6a46",
   "metadata": {},
   "source": [
    "![Initial conditions](Acoustic_2D_start.png)\n",
    "\n",
    "# Solution \n",
    "\n",
    "The solution is rather straightforward:"
   ]
  },
  {
   "cell_type": "raw",
   "id": "4be51617-eba0-44e0-a3ca-07af10c8f157",
   "metadata": {},
   "source": [
    "     double ::exahype2::...::maxEigenvalue(\n",
    "      ...\n",
    "     ) {\n",
    "       const double K0   = 4.0;\n",
    "       const double rho  = 1.0;\n",
    "       const double c    = std::sqrt(4.0 / rho);\n",
    "\n",
    "       return c;\n",
    "     }\n",
    "\n",
    "\n",
    "     void ::exahype2::...::flux(\n",
    "      ...\n",
    "     ) {\n",
    "       const double K0 = 4.0, rho = 1.0;\n",
    "\n",
    "       switch(normal) {\n",
    "         case 0: // Flux in x-direction\n",
    "           F[0]  = K0 * Q[1];\n",
    "           F[1]  = Q[0] / rho;\n",
    "           F[2]  = 0.0;\n",
    "           break;\n",
    "         case 1: // Flux in y-direction\n",
    "           F[0]  = K0 * Q[2];\n",
    "           F[1]  = 0.0;\n",
    "           F[2]  = Q[0] / rho;\n",
    "       }\n",
    "     }\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
