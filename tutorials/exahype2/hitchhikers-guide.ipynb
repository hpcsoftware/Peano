{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f2bdaac6-5239-4a2b-aaf7-516885df6846",
   "metadata": {},
   "source": [
    "# ExaHyPE 2 - Hitchhiker's guide\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4cd50580-8a60-48d5-b0ff-386d7532eea4",
   "metadata": {},
   "source": [
    "This tutorial runs through the basic steps of any ExaHyPE 2 application. For this, we assume that you have followed the @ref page_installation_home \"installation instructions\" already. \n",
    "\n",
    "## Dependencies\n",
    "\n",
    "We furthermore assume that the following extensions have been added:"
   ]
  },
  {
   "cell_type": "raw",
   "id": "afb41a7f-a342-4ae8-b23a-2315e0f3c593",
   "metadata": {},
   "source": [
    "     --enable-blockstructured --enable-exahype"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "617c094b-78b1-417e-9416-10303808db7d",
   "metadata": {},
   "source": [
    "If you want to have a parallel code, you furthermore need"
   ]
  },
  {
   "cell_type": "raw",
   "id": "a3a7910e-ec1a-42a4-b385-f82a2b57ebc8",
   "metadata": {},
   "source": [
    "     --enable-loadbalancing"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4b58b52-dffc-4413-8e8b-6fbee5a134a1",
   "metadata": {},
   "source": [
    "## CMake\n",
    "\n",
    "If you prefer CMake, ensure that the extensions from above are activated. You might want to invoke ```ccmake``` command within your build directory and check before you continue. Once you toggle them on (press space), rerun make, so you all your Peano and ExaHyPE libraries are built properly. For more advanced users, you can activate the recommended packages on the command line. From hereon, ensure that you use the Jupyter notebook is called from the tutorials subdirectory hosted ***within your build directory***. So not use the one from the source directory, as pathes otherwise will be messed up. Follow-up notebooks all refer to autotools settings.\n",
    "\n",
    "## Getting started\n",
    "\n",
    "Typically, we kick off with a new director for our project. Within this directory, we start with a plain Python script. In this guide, we work with a Python notebook rather than a script. ExaHyPE codes need to know where to find the ExaHyPE and Peano Python sources. For this, we can either add the Python sources to our environment, or export the path variable prior to invoking our script/kicking off our Jupyter notebook, or we set the path hard-coded in our script. To be honest, this last approach is likely the least elegant one, as it hard-codes the pathes, but it works:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7c81d32e-e7aa-4b6b-a0b9-f12e28bbeba2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys, os\n",
    " \n",
    "sys.path.insert(0, os.path.abspath(\"../../python/\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19c143e7-3bee-4cca-83d8-a4f18c70e19b",
   "metadata": {},
   "source": [
    "Every ExaHyPE project starts with the obligatory imports:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4e529609-072e-4124-98ae-5290b58fc49c",
   "metadata": {},
   "outputs": [],
   "source": [
    "import peano4, exahype2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef16b01c-5c84-40b5-9675-c7669ba34ffc",
   "metadata": {},
   "source": [
    "At this very moment, you will get a lot of warnings. They are invalid warnings in the sense that Peano and ExaHyPE both use LaTeX formulae quite excessively in their in-code documentation, and Jupyter notebooks don't like that. In a plain Python script, all will be fine. If you get errors re the inavailability of Paraview or VTK, that's fine too. After all, we cannot expect most Jupyter installations to host an embedded Paraview/VTK server."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cb43af9f-f7f3-4aa5-833a-40378ad95fd4",
   "metadata": {},
   "source": [
    "## Creating an ExaHyPE and Peano project\n",
    "\n",
    "Every ExaHyPE project defines a sequence of lowering steps. That is, we start with an ExaHyPE project object. This represents the domain level, where we do the math. After that we lower this project into a Peano project. This is a technical representation (still in Python) of the arising algorithm, i.e. something that represents the actual code base realising our domain knowledge. Then we ask Peano to lower this project into native C++ code plus a Makefile. \n",
    "\n",
    "![Lowering process](illustrations/generic-python-workflow.png)\n",
    "\n",
    "The term lowering stems from the compiler community where they use it to argue about different domain-specific languages and their abstraction levels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8c0828fc-43f3-45fc-ae3e-9daa437fef92",
   "metadata": {},
   "outputs": [],
   "source": [
    "project = exahype2.Project(\n",
    "    [\"tutorials\", \"exahype2\"],\n",
    "    \"hitchhiker\",\n",
    "    executable=\"my-code\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68493a2f-fdc1-4744-956a-ae3657e1890e",
   "metadata": {},
   "source": [
    "This creates the domain-specific project. What follows is the lowering. This lowering happens at the end of the setup script typically. We demonstrate it once here to explain the process, even though this will give us a totally empty meaningless project, as we haven't yet added any domain-specific logic:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f7cfc684-86af-4769-8d06-cc47ad0556c7",
   "metadata": {},
   "outputs": [],
   "source": [
    "project.set_Peano4_installation( \"../..\", peano4.output.CompileMode.Asserts )\n",
    "\n",
    "peano4_project = project.generate_Peano4_project()\n",
    "peano4_project.generate()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb8e72d4-db6e-4c7f-8964-1c4dfbf94f69",
   "metadata": {},
   "source": [
    "We first get some output that stems from the set_Peano4_installation() call. This one parses all the flags you handed into CMake or the configure script, respectively, and picks them up. It also looks which static libraries have been built and so forth. So from hereon, the project \"knows\" what your installation looks like. It will also run some primitive sanity checks to see if all the required extensions are activtated. \n",
    "\n",
    "Once completed, we lower the domain-specific object into a Peano 4 object. We can still add further content on this abstraction level (e.g. external libraries can be added here), but eventually we call generate(). This makes the code dump a lot of glue file which is at this point largely empty, as we haven't added any functionality yet. Later on, the code will automatically befill them with something meaningful. We end up with a complete C++ code base with all of our functionality plus a plain old Makefile to build our actual code: \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "591bad03-349e-4c11-bf63-a92edebe58da",
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a37f0ae1-2981-4d91-abd2-fa058bf5a3a4",
   "metadata": {},
   "source": [
    "shows us that we now have a Makefile, which we can either use on the command line, or we trigger the make from through a Python subprocess call. The generated code that is now scattered over all the generated subdirectories follows the generic @ref page_architecture_home of any code built on top of Peano:\n",
    "\n",
    "\n",
    "![System architecture](illustrations/architecture-static.png)\n",
    "\n",
    "\n",
    "A domain-specific code sits on top of some extensions plus the Peano core, and all of the components use a common technical architecture. Third-party libraries are either hidden by the tarch, or they are plugged into the domain-specific code. We don't use any so far."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "22676793-fde4-48bf-b078-f117a7fef0ec",
   "metadata": {},
   "source": [
    "## Configure the global simulation\n",
    "\n",
    "Every single ExaHyPE project adds solvers before it lowers the project into a Peano 4 project, and also sets some simulation parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5c4b4ac3-5ea4-4b8e-b504-f1fb86e2c7e8",
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "cube_size = 1.0\n",
    "dimensions = 3\n",
    "project.set_global_simulation_parameters(\n",
    "  dimensions            = dimensions,\n",
    "  offset                = [ 0.0       for _ in range(dimensions) ],\n",
    "  size                  = [ cube_size for _ in range(dimensions) ],\n",
    "  min_end_time          = 1.0,\n",
    "  max_end_time          = 1.0,\n",
    "  first_plot_time_stamp = 0.0,\n",
    "  time_in_between_plots = 0.01,\n",
    "  periodic_BC           = [True, True, True],\n",
    ")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "599aaa09-2eed-4da9-b311-5c5afead3f00",
   "metadata": {},
   "source": [
    "By default, we work on the unit cube, i.e. a 3d setup. Obviously, you will want to change this usually. Note that the global parameters have to be set **before** you lower the code into C/C++, so it comes slightly too late here. \n",
    "\n",
    "The other thing that we have to do before we lower into C++ is to add the actual solvers. So far, the project is empty and does nothing. This adding of individual solvers is then discussed next. Before we do so, let's get some workflow right.\n",
    "\n",
    "## Visualisation\n",
    "\n",
    "Peano writes its own bespoke data file format. There are some scripts delivered to convert them into VTK which you can load into Paraview or VisIt, e.g. A lot of applications also provide their own scripts to analyse the output. In theory, you can embed all of this postprocessing into Jupyter notebooks, too. With Paraview, this can be a little bit fiddly. With other postprocessing, the normal workflow is that you build the code (see remark on Make above), then you run the code on the terminal through the Python notebook (cmp the use of the ls command above) and you pipe the outcome into a text file. You can then invoke the data postprocessing scripts of your choice directly within the notebook on the dumped file and plot the outcome in-situ. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "09b7372e-be5c-4472-9c3d-b5ac86c5c9ce",
   "metadata": {},
   "source": [
    "## Exercises\n",
    "\n",
    "(1) Run through the steps in the tutorial and type in "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0f22db75-38a9-40cf-9d86-df8ca23839e6",
   "metadata": {},
   "outputs": [],
   "source": [
    "!make"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a35e8e5-a48c-4076-8c0c-199a2dfe01df",
   "metadata": {},
   "source": [
    "(2) If the project has compiled successfully, have a look what the executable is called. Change this name in the project setting and look up the signature of set_global_simulation_parameters(). For this, you might want to consult https://hpcsoftware.pages.gitlab.lrz.de/Peano/ and just search for the function in the search field. Note that multiple projects define such a function, so you have to pick the docu of ExaHyPE's project. Recompile and then run the executable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "88f044a8-5b27-4294-9e4f-af58428876f0",
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "239bcf0d-ce96-4362-b4c9-32036e70daf5",
   "metadata": {},
   "source": [
    "(3) The code will dump quite a lot of output. For longer runs, we will want to filter out these dumps. It is therefore time to study the page @ref tarch_logging of the tarch documentation. ExaHyPE is ready to use such a file - just create a log filter file called exahype2.log-filter in the directory and modify it accordingly. If you search the repository, you will see a lot of example log files. You don't have to write one from scratch. Just copy it over and modify it. Rerun the code.\n",
    "(4) Finally, search for outputs. There are no outputs (yet). This makes sense: After all, we haven't added a solver so far. Time to continue with the next tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1db1851f-c97e-4098-a4db-9faa1de8fb44",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
