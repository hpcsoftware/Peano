{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f2bdaac6-5239-4a2b-aaf7-516885df6846",
   "metadata": {},
   "source": [
    "# Tutorial 1: Matrix-free Continuous Galerkin single-level solver on a regular grid\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "87092c99-0a86-4ff3-8c27-f9b4c1a9c900",
   "metadata": {},
   "source": [
    "This tutorial runs through the basic steps how to construct a geometric single level Jacobi solver for \n",
    "\n",
    "$$ -\\Delta u = f $$\n",
    "\n",
    "While noone would use such a numerical scheme as a real solver for any problem of relevant size, this type of solver often is a building block of proper multigrid algorithms."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9930cfb0-ca88-431f-9f4e-700d6648bcf1",
   "metadata": {},
   "source": [
    "## Prepare the environment \n",
    "\n",
    "MGHyPE codes need to know where to find the MGHyPE and Peano Python sources. For this, we can either add the Python sources to our environment, or export the path variable prior to invoking our script/kicking off our Jupyter notebook, or we set the path hard-coded in our script. To be honest, this last approach is likely the least elegant one, as it hard-codes the pathes, but it works:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cbd37d4d-368f-4669-b4c3-c999173382b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys, os\n",
    " \n",
    "sys.path.insert(0, os.path.abspath(\"../../../python/\"))\n",
    "sys.path.insert(0, os.path.abspath(\"../../../src/\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a65b4ad1-0e1a-44c9-990e-baa250e8371e",
   "metadata": {},
   "source": [
    "Once we have ensured that all pathes are correct, we import the packages of interest:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "52829ae4-cb0c-469f-b771-2e0e224aa93e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import peano4, mghype"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a0efe12a-d1ea-4304-8d54-d4bb049c8b6a",
   "metadata": {},
   "source": [
    "## An MGHyPE project\n",
    "\n",
    "Every MGHyPE project starts with an MGHyPE project object: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "477831c5-e58e-4d32-9a2c-5b74c7fd3338",
   "metadata": {},
   "outputs": [],
   "source": [
    "project = mghype.matrixfree.api.Project( project_name = \"Tutorial01\", \n",
    "                                         namespace = [ \"tutorials\", \"mghype\", \"examples\" ],\n",
    "                                         executable = \"tutorial01\",\n",
    "                                        )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a76909f5-c94d-48dc-a92b-beb5cefa802f",
   "metadata": {},
   "source": [
    "This creates the domain-specific project. We immediately set the domain properties:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c7a7e6d5-a40b-4369-8366-c302ae21b876",
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "cube_size = 1.0\n",
    "dimensions = 2\n",
    "project.set_global_simulation_parameters(\n",
    "  dimensions            = dimensions,\n",
    "  offset                = [ 0.0       for _ in range(dimensions) ],\n",
    "  domain_size           = [ cube_size for _ in range(dimensions) ],\n",
    "  plot_after_each_mesh_sweep = False,\n",
    "  max_iterations             = 20\n",
    ")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f0eba67d-eb7b-421a-b635-ca608c5b5117",
   "metadata": {},
   "source": [
    "We parse the system setup next, so all the compiler settings and stuff is known to MGHyPE/Peano. As we develop code, we switch to asserts mode, which means a lot of checks are injected into the code base. Once we are sure everything works, we toggle this switch to Release:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3a8d25e6-9838-4c77-b896-7d8780481c47",
   "metadata": {},
   "outputs": [],
   "source": [
    "project.set_Peano4_installation( \"../../..\", peano4.output.CompileMode.Asserts )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a14ebe31-b881-4fed-9ded-be6bcebd5352",
   "metadata": {},
   "source": [
    "## Add a solver\n",
    "\n",
    "Once we have a project, we can start to construct our solver. One of the first things that we'll gonna need is the element assembly matrices. MGHyPE always assumes that the discretisation yields a system\n",
    "\n",
    "$$ Ax = Mb $$\n",
    "\n",
    "with a system matrix A and a right-hand side matrix M. If we employ the scheme as an actual solver, this M is the mass matrix corresponding to an integral\n",
    "\n",
    "$$ \\int (f, \\varphi ) dx $$\n",
    "\n",
    "Furthermore, both A and M are always handed over to the solver as normalised matrics (over the unit cube) plus some h-scaling. There's no need to create these matrices manually. We can use one of MGHyPE's factory mechanisms:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "72794a18-e257-4ff3-a1fc-3a9639847a2a",
   "metadata": {},
   "outputs": [],
   "source": [
    "poisson_equation_matrices = mghype.api.matrixgenerators.DLinear(\n",
    "  dimensions = dimensions,\n",
    "  unknowns_per_vertex_dof = 1\n",
    ")\n",
    "\n",
    "system_matrix, system_matrix_scaling = poisson_equation_matrices.get_cell_system_matrix_for_laplacian()\n",
    "mass_matrix,   mass_matrix_scaling   = poisson_equation_matrices.get_cell_mass_matrix()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0f82df93-6eba-4b81-a4b0-da17e8e09a28",
   "metadata": {},
   "source": [
    "We briefly inspect the results (something we would likely not do for a \"real\" simulation):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "af7c31f9-7214-405b-be75-4b4e0aa98eb0",
   "metadata": {},
   "outputs": [],
   "source": [
    "print( system_matrix )\n",
    "print( system_matrix_scaling)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "984199e2-6575-4632-b7cc-37686516b93e",
   "metadata": {},
   "outputs": [],
   "source": [
    "print( mass_matrix )\n",
    "print( mass_matrix_scaling )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "60263232-4a9f-4293-9aa4-bf2df363039b",
   "metadata": {},
   "source": [
    "The first matrix is the standard (2d) element matrix for the Poisson equation. The scaling array indicates that this one has to be scaled with $$h^0$$ before we continue. More details can be found in the documentation of these routines on the webpage. When we study the mass matrix, there are no big surprises either. \n",
    "\n",
    "Next we create the solver using these matrices and add it to the project:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "432d9d66-d19f-4727-991c-01629816da6b",
   "metadata": {},
   "outputs": [],
   "source": [
    "solver_name = \"CollocatedPoisson\"\n",
    "mesh_size = 0.1\n",
    "solver = mghype.matrixfree.solvers.api.CollocatedDLinearDiscretisationWithPointJacobi(solver_name,\n",
    "                                                              1, # unknowns per vertex, i.e. scalar equation\n",
    "                                                              dimensions,\n",
    "                                                              mesh_size,\n",
    "                                                              mesh_size,\n",
    "                                                              local_assembly_matrix = system_matrix,\n",
    "                                                              local_assembly_matrix_scaling=system_matrix_scaling,\n",
    "                                                              mass_matrix = mass_matrix,\n",
    "                                                              mass_matrix_scaling = mass_matrix_scaling,\n",
    "                                                              solver_tolerance=0.01\n",
    "                                                            )\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eac8e2a6-3c77-492c-9141-d5d8afc02f5b",
   "metadata": {},
   "outputs": [],
   "source": [
    "project.add_solver(solver)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dffb1d52-27bf-46da-949e-3a611c7b6ae2",
   "metadata": {},
   "source": [
    "## Create and build project\n",
    "\n",
    "We have now set up the solver. Hence, we can lower it to C++: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a931773f-bbdc-440a-9306-ea46b3df9b2b",
   "metadata": {},
   "outputs": [],
   "source": [
    "peano4_project = project.generate_Peano4_project(verbose=True)\n",
    "peano4_project.generate()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32557698-87c4-468a-942f-ff73b89aefce",
   "metadata": {},
   "source": [
    "We build the project now:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22271ca1-6851-4285-b2b1-f513de6d0375",
   "metadata": {},
   "outputs": [],
   "source": [
    "!make -j8"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0df399db-5690-4e98-b5d5-13dc70cab76a",
   "metadata": {},
   "source": [
    "So far, this is a disfunctional solver, as we haven't set any boundary conditions, and no right-hand side. There are multiple ways to do this in MGHyPE. We demonstrate the low-level version here: Every solver is mapped onto an AbstractCollocatedPoisson class and a subclass called CollocatedPoisson. The abstract class is regenerated every time we run Python and basically stores all the data from the Python within the C++ code. It represents a translation from Python into the compiled world. The subclass can be used by us to tailor the code to our actual needs.\n",
    "\n",
    "# Exercise\n",
    "\n",
    "Open the C++ file of the solver and ensure that it realises a proper solver, i.e. something alike"
   ]
  },
  {
   "cell_type": "raw",
   "id": "137d4efd-2483-4a2e-b795-7c59f51685ee",
   "metadata": {},
   "source": [
    "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",
    "#include <cmath>\n",
    "\n",
    "\n",
    "tutorials::mghype::examples::CollocatedPoisson::CollocatedPoisson() {\n",
    "}\n",
    "\n",
    "\n",
    "tutorials::mghype::examples::CollocatedPoisson::~CollocatedPoisson() {\n",
    "}\n",
    "\n",
    "\n",
    "void tutorials::mghype::examples::CollocatedPoisson::initVertex(\n",
    "  const tarch::la::Vector<Dimensions, double>&  x,\n",
    "  const tarch::la::Vector<Dimensions, double>&  h,\n",
    "  tarch::la::Vector< 1, double >&  value,\n",
    "  tarch::la::Vector< 1, double >&  rhs\n",
    ") {\n",
    "  value = 0.0;\n",
    "  rhs   = 1.0;\n",
    "  for (int d=0; d<Dimensions; d++) {\n",
    "    rhs *= std::sin( tarch::la::PI * x(d) );\n",
    "  } \n",
    "}\n",
    "\n",
    "void tutorials::mghype::examples::CollocatedPoisson::setBoundaryConditions(\n",
    "  const tarch::la::Vector<Dimensions, double>&  x,\n",
    "  const tarch::la::Vector<Dimensions, double>&  h,\n",
    "  tarch::la::Vector< 1, double >&  value\n",
    ") {\n",
    "  value = 0.0;\n",
    "}\n",
    "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37088555-a4ca-46e4-9be7-378f2571f6d1",
   "metadata": {},
   "source": [
    "## Running the experiment\n",
    "\n",
    "We next run the experiment but ensure that the output is piped into a file, as we'll later on use this dump to run some analysis:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2a5097c8-498e-404d-8bf9-73bb24b57b29",
   "metadata": {},
   "outputs": [],
   "source": [
    "!rm *.peano-patch-file\n",
    "!./tutorial01 > tutorial01.out\n",
    "!cat tutorial01.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b7c43a8-343a-4a66-bf3c-31c60c59a6c0",
   "metadata": {},
   "source": [
    "Nothing controversial here. The residual goes down albeit slowly, and we therefore never reach out numerical termination criterion. Instead, we quit once we exceed the maximum number of iterations."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "69d97701-25d1-4867-8799-4b41e1be3cf0",
   "metadata": {},
   "source": [
    "## Visualisation\n",
    "\n",
    "Peano writes its own bespoke data file format. There are some scripts delivered to convert them into VTK which you can load into Paraview or VisIt, e.g. We use exactly this script here to convert the outcome into a standard visualisation format. Unfortunately, my pvpython sometimes seems to miss out on environment variables. So I have to re-add those of interest manually:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20b8360b-b9e8-4b62-993a-10ceed94bcd1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import subprocess\n",
    "import os\n",
    "my_env = os.environ\n",
    "my_env[\"PYTHONPATH\"] = \"../../../python:\" + my_env[\"PYTHONPATH\"]\n",
    "subprocess.call([\"pvpython\", \"../../../python/peano4/visualisation/render.py\", \"CollocatedPoisson.rhs.peano-patch-file\"], env=my_env)\n",
    "subprocess.call([\"pvpython\", \"../../../python/peano4/visualisation/render.py\", \"CollocatedPoisson.value.peano-patch-file\"], env=my_env)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b19ef084-a345-468e-875a-7ad4b2a35837",
   "metadata": {},
   "source": [
    "## Display the outcome\n",
    "\n",
    "We can now visualise the pvd/vtu files in Paraview or VisIt. As this is all only 2d, I use the PyVista package to do this directly within the Jupyter notebook. The first step grabs the package and therefore is required once and once only. The second one then creates the pics:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12138e0c-93f2-428a-bdac-ff877a76bf62",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "!{sys.executable} -m pip install --break-system-packages pyvista[all]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4af21ed7-0594-4567-842e-a6d2cb173c2f",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyvista\n",
    "mesh = pyvista.read( \"CollocatedPoisson.rhs.pvd\" )\n",
    "mesh.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "53bb5aff-da92-43e1-85e2-db523233af60",
   "metadata": {},
   "source": [
    "No surprises with the right-hand side. We however see that the result is nowhere near the real solution. This is due to the poor convergence rates. In the script below, we might want to pass in ```clim=[from,to]``` to limit the range displayed. Note furthermore that we pick a ```second``` time step here, and do not load the pvd file. Each solver, by default, writes the initial solution and the last one - unless we make it plot every single mesh sweep."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30179c38-a743-4d6f-bace-e4853f02770c",
   "metadata": {},
   "outputs": [],
   "source": [
    "plotter = pyvista.Plotter()\n",
    "mesh = pyvista.read( \"CollocatedPoisson.value.peano-patch-file-1.vtu\" )\n",
    "plotter.add_mesh(mesh, show_edges=True ) # , clim=[0.0, 0.01]\n",
    "plotter.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7aa97a85-cfde-4c09-aa53-ba3616e5dd2f",
   "metadata": {},
   "source": [
    "## Study the convergence behaviour\n",
    "\n",
    "We close this first tutorial with a plot of the convergence behaviour. For this, we open the output that we'd streamed into a text file and parse the results. As the dumps are kind of standardised, we can use some utility functions shipped with MGHyPE to do the actual parsing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0b26688e-30b0-4b66-bd57-661d72c57691",
   "metadata": {},
   "outputs": [],
   "source": [
    "res_max, res_2, res_h = mghype.api.postprocessing.parse_residuals( \"tutorial01.out\", solver_name )\n",
    "du_max, du_2, du_h = mghype.api.postprocessing.parse_solution_updates( \"tutorial01.out\", solver_name )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4f9eb19f-a29d-48aa-85fe-bc52b2cd0038",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "plt.plot(res_2,   label='$|res|_2$',       marker='<')\n",
    "plt.plot(du_2,    label='$|\\delta u|_2$',  marker='>')\n",
    "#plt.plot(res_max, label='$|res|_{max}$', marker='>')\n",
    "#plt.plot(res_h,   label='$|res|_h$',     marker='v')\n",
    "plt.yscale('log')\n",
    "plt.xlabel('iterations')\n",
    "plt.ylabel('residual')\n",
    "plt.legend()\n",
    "plt.savefig( \"tutorial01.pdf\" )\n",
    "plt.savefig( \"tutorial01.png\" )\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "210e8562-63bc-4b62-9d6e-557a0dec8435",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
