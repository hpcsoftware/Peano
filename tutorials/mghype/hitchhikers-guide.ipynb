{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f2bdaac6-5239-4a2b-aaf7-516885df6846",
   "metadata": {},
   "source": [
    "# MGHyPE - Hitchhiker's guide\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "87092c99-0a86-4ff3-8c27-f9b4c1a9c900",
   "metadata": {},
   "source": [
    "This tutorial runs through the basic steps of any multigrid application. For this, we assume that you have followed the @ref page_installation_home \"installation instructions\" already. We furthermore assume that the following extensions have been added:"
   ]
  },
  {
   "cell_type": "raw",
   "id": "45b937eb-e484-432e-93cd-3b9eee99828d",
   "metadata": {},
   "source": [
    "     --enable-blockstructured --enable-particles --enable-loadbalancing --enable-finiteelements --enable-mghype --enable-swift --enable-exahype"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "00e2b0a1-ee3d-4817-8d00-bbd0dc9618d8",
   "metadata": {},
   "source": [
    "If you have a parallel code, you furthermore need"
   ]
  },
  {
   "cell_type": "raw",
   "id": "4d3bb8e2-f038-459e-b328-447659c0b65e",
   "metadata": {},
   "source": [
    "     --enable-loadbalancing"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9930cfb0-ca88-431f-9f4e-700d6648bcf1",
   "metadata": {},
   "source": [
    "This is for domain decomposition as we discuss later. Typically, we kick off with a new director for our project. Within this directory, we start with a plain Python script. In this guide, we work with a Jupyter notebook rather than a script. MGHyPE codes need to know where to find the MGHyPE and Peano Python sources. For this, we can either add the Python sources to our environment, or export the path variable prior to invoking our script/kicking off our Jupyter notebook, or we set the path hard-coded in our script. To be honest, this last approach is likely the least elegant one, as it hard-codes the pathes, but it works:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cbd37d4d-368f-4669-b4c3-c999173382b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys, os\n",
    " \n",
    "sys.path.insert(0, os.path.abspath(\"../../python/\"))\n",
    "sys.path.insert(0, os.path.abspath(\"../../src/\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a65b4ad1-0e1a-44c9-990e-baa250e8371e",
   "metadata": {},
   "source": [
    "Our code atm is not super-clean. On the long run, we want to hold all codes, i.e. C++ and Python, within the src directory. For historical reasons, we still have the C++ code mainly in src, and the majority of the Peano code base in python. Only the multigrid Python ingredients are already moved over into src.\n",
    "\n",
    "Once we have ensured that all pathes are correct, every project built with MGHyPE starts with the same imports:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "52829ae4-cb0c-469f-b771-2e0e224aa93e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import peano4, mghype"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a0efe12a-d1ea-4304-8d54-d4bb049c8b6a",
   "metadata": {},
   "source": [
    "## An MGHyPE project and its lowering into a Peano project\n",
    "\n",
    "Every MGHyPE project defines a sequence of lowering steps. That is, we start with an MGHyPE project object. This represents the domain level, where we do the math. After that we lower this project into a Peano project. This is a technical representation (still in Python) of the arising algorithm, i.e. something that represents the actual code base realising our domain knowledge. Then we ask Peano to lower this project into native C++ code plus a Makefile. \n",
    "\n",
    "![Lowering process](illustrations/generic-python-workflow.png)\n",
    "\n",
    "The term lowering stems from the compiler community where they use it to argue about different domain-specific languages and their abstraction levels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "477831c5-e58e-4d32-9a2c-5b74c7fd3338",
   "metadata": {},
   "outputs": [],
   "source": [
    "project = mghype.matrixfree.api.Project( project_name = \"MyProject\", \n",
    "                              namespace = [ \"tutorials\", \"mghype\" ]\n",
    "                            )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a76909f5-c94d-48dc-a92b-beb5cefa802f",
   "metadata": {},
   "source": [
    "This creates the domain-specific project. What follows is the lowering. This lowering happens at the end of the setup script typically. We demonstrate it once here to explain the process, even though this will give us a totally empty meaningless project, as we haven't yet added any domain-specific logic:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "04e24f4b-821b-4d3f-95af-4d3ce31bdbf6",
   "metadata": {},
   "outputs": [],
   "source": [
    "project.set_Peano4_installation( \"../..\", peano4.output.CompileMode.Asserts )\n",
    "\n",
    "peano4_project = project.generate_Peano4_project()\n",
    "peano4_project.generate()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f0eba67d-eb7b-421a-b635-ca608c5b5117",
   "metadata": {},
   "source": [
    "We first get some output that stems from the set_Peano4_installation() call. This one parses all the flags you handed into CMake or the configure script, respectively, and picks them up. It also looks which static libraries have been built and so forth. So from hereon, the project \"knows\" what your installation looks like. It will also run some primitive sanity checks to see if all the required extensions are activtated. \n",
    "\n",
    "Once completed, we lower the domain-specific object into a Peano 4 object. We can still add further content on this abstraction level (e.g. external libraries can be added here), but eventually we call generate(). This makes the code dump a lot of glue file which is at this point largely empty, as we haven't added any functionality yet. Later on, the code will automatically befill them with something meaningful. We end up with a complete C++ code base with all of our functionality plus a plain old Makefile to build our actual code: \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "abffa94c-a2d8-4d5f-9a54-094595e8b139",
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "795c4c0d-3c0f-4a1a-b74a-927ae1ce16c3",
   "metadata": {},
   "source": [
    "shows us that we now have a Makefile, which we can either use on the command line, or we trigger the make from through a Python subprocess call. The generated code that is now scattered over all the generated subdirectories follows the generic @ref page_architecture_home of any code built on top of Peano:\n",
    "\n",
    "\n",
    "![System architecture](illustrations/architecture-static.png)\n",
    "\n",
    "A domain-specific code sits on top of some extensions plus the Peano core, and all of the components use a common technical architecture. Third-party libraries are either hidden by the tarch, or they are plugged into the domain-specific code. We don't use any so far."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a14ebe31-b881-4fed-9ded-be6bcebd5352",
   "metadata": {},
   "source": [
    "## Add the functionality\n",
    "\n",
    "Every single multigrid project adds solvers before it lowers the project into a Peano 4 project, and also sets some simulation parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c7a7e6d5-a40b-4369-8366-c302ae21b876",
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "cube_size = 1.0\n",
    "dimensions = 3\n",
    "project.set_global_simulation_parameters(\n",
    "  dimensions            = dimensions,\n",
    "  offset                = [ 0.0       for _ in range(dimensions) ],\n",
    "  domain_size           = [ cube_size for _ in range(dimensions) ],\n",
    "  plot_each_timestep    = True,\n",
    ")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "69d97701-25d1-4867-8799-4b41e1be3cf0",
   "metadata": {},
   "source": [
    "You obviously have to do this prior to the lowering into C++. And then there's the actual solver missing. That is, at the moment nothing will happen if you compile this project.\n",
    "\n",
    "## Visualisation\n",
    "\n",
    "Peano writes its own bespoke data file format. There are some scripts delivered to convert them into VTK which you can load into Paraview or VisIt, e.g. A lot of applications also provide their own scripts to analyse the output. In theory, you can embed all of this postprocessing into Jupyter notebooks, too. With Paraview, this can be a little bit fiddly. With other postprocessing, the normal workflow is that you build the code (see remark on Make above), then you run the code on the terminal through the Python notebook (cmp the use of the ls command above) and you pipe the outcome into a text file. You can then invoke the data postprocessing scripts of your choice directly within the notebook on the dumped file and plot the outcome in-situ. We will look into this in a minute.\n",
    "\n",
    "## Exercises\n",
    "\n",
    "(1) Run through the steps above and then compile the arising code. Peano's project offers a function to this through its API. If you work with Jupyter notebooks, you can also do it directly from within the notebook:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20b8360b-b9e8-4b62-993a-10ceed94bcd1",
   "metadata": {},
   "outputs": [],
   "source": [
    "!make"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0509387d-2f6f-42fc-9c93-2623cd6a5c95",
   "metadata": {},
   "source": [
    "(2) Make an ls on the directory and then run the executable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9a793508-e31b-45af-9749-4e21f97cf3b0",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "fc6e815a-7187-44ff-bf9b-9078002763fb",
   "metadata": {},
   "source": [
    "(3) The code will dump quite a lot of output. For longer runs, we will want to filter out these dumps. It is therefore time to study the page @ref tarch_logging  of the tarch documentation. MGHyPE is ready to use such a file - just create a log filter file called mghype.log-filter in the directory and modify it accordingly. If you search the repository, you will see a lot of example log files. You don't have to write one from scratch. Just copy it over and modify it. Rerun the code. \n",
    "\n",
    "(4) Finally, search for outputs. There are no outputs (yet). This makes sense: After all, we haven't added a solver so far. Time to continue with the next tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c3cf7827-2538-48bb-8477-a9f4de7c673f",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
