/**

@page applications_exahype2_elastic Elastic

# How to Build and Run

For a general overview and all options on how to retrieve and configure the project, refer to @ref page_installation_home.

## Using CMake

    cd myPeanoDirectory
    pip3 install -e .
    mkdir build && cd build
    CC=gcc CXX=g++ cmake .. -DENABLE_EXAHYPE=ON -DENABLE_LOADBALANCING=ON -DENABLE_BLOCKSTRUCTURED=ON -DWITH_MULTITHREADING=omp
    make -j && make test
    cd ../applications/exahype2/elastic/scenario/
    python3 scenario.py
    ./Elastic.Release

## Using Automake

    cd myPeanoDirectory
    pip3 install -e .
    libtoolize; aclocal; autoconf; autoheader; cp src/config.h.in .; automake --add-missing
    CC=gcc CXX=g++ ./configure --enable-exahype --enable-loadbalancing --enable-blockstructured --with-multithreading=omp CXXFLAGS="-std=c++20 -O3 -fopenmp -Wno-attributes" LDFLAGS="-fopenmp"
    make -j && make check
    cd applications/exahype2/elastic/scenario/
    python3 scenario.py
    ./Elastic.Release

Where `CC=gcc CXX=g++` can be any set of compilers.

# Visualise the Simulation Output

    python3 myPeanoDirectory/python/peano4/visualisation/render.py solution/solution-Elastic.peano-patch-file

# Scenarios

\li \subpage applications_exahype2_elastic_point_explosion

*/
