/**

@page applications_exahype2_shallow_water_dam_break_landslide Landslide Dam Break on an Inclined Plane

In one spatial dimension, the equations takes the form

$$
h_t + (hu)_x = 0,
\quad (hu)_t + (hu^2 + \frac{1}{2}gh^2)_x = ghb_x - \frac{1}{\rho}(f_C + f_V),
$$

where @f$ h, v @f$ is the depth and velocity of the flow and @f$ b @f$ is the topography and @f$ g @f$ is gravity.
The friction forces are represented by the Coulomb friction @f$ f_C @f$ and the Voellmy friction @f$ f_V @f$ defined according to

$$
f_C = \mu \rho gh\cos(\theta)\frac{u}{|u|}, \quad f_V = \rho \frac{g}{\xi}|u|u,
$$

where @f$ \theta @f$ is the slope angle of the topography, @f$ \mu = \tan(\phi) @f$, where @f$ \phi @f$ is the (basal) friction angle and @f$ \xi @f$ is the Voellmy friction parameter.

The full 2D equations including more complicated friction laws and material-dependent properties are given for an inclined plane by

\f{eqnarray*}{
\frac{\partial}{\partial t} \begin{pmatrix}
    h\\hu\\hv
    \end{pmatrix}
    + \frac{\partial}{\partial x}
    \begin{pmatrix}
    hu\\hu^2 + \frac{1}{2} \cdot g\cos(\zeta)h^2\\huv
    \end{pmatrix}
    + \frac{\partial}{\partial y}
    \begin{pmatrix}
    hv\\huv\\hv^2 + \frac{1}{2} \cdot g\cos(\zeta)h^2
    \end{pmatrix}
    +D(x) + D(y) =
    \begin{pmatrix}
    0\\hg\sin(\zeta)-\mu\frac{u}{\sqrt{u^2+v^2}}hg\cos(\zeta)\\-\mu\frac{v}{\sqrt{u^2+v^2}}hg\cos(\zeta)
\end{pmatrix},
\f}

and

\f{eqnarray*}{
D(x)=\begin{pmatrix}
    0\\
    -\nu\sqrt{h}(1.5\cdot\frac{\partial h}{\partial x}\frac{\partial u}{\partial x} + h\frac{\partial^2 u}{\partial x^2})\\
    -0.5\cdot\nu\sqrt{h}(1.5\cdot\frac{\partial h}{\partial x}(\frac{\partial u}{\partial y}+\frac{\partial v}{\partial x})+h(\frac{\partial u}{\partial y \partial x}+\frac{\partial^2 v}{\partial x^2}))
\end{pmatrix},
\f}

\f{eqnarray*}{
D(y)=\begin{pmatrix}
    0\\
    -0.5\cdot\nu\sqrt{h}(1.5\cdot\frac{\partial h}{\partial y}(\frac{\partial u}{\partial y}+\frac{\partial v}{\partial x})+h(\frac{\partial^2 u}{\partial y^2}+\frac{\partial v}{\partial x \partial y}))\\
    -\nu\sqrt{h}(1.5\cdot\frac{\partial h}{\partial y}\frac{\partial v}{\partial y} + h\frac{\partial^2 v}{\partial y^2})
\end{pmatrix},
\f}

where @f$ \zeta @f$ is the angle of inclination of the inclined plane.

The coefficient of basal friction @f$ \mu(Fr,h) @f$ is a piecewise function
and the coefficient of viscosity @f$ \nu @f$ is a function of the material parameters.
The complete expression for @f$ \mu(Fr,h) @f$ and @f$ \nu @f$ can be found in literature.

The ncp term requires the calculation of partial derivatives of @f$ h @f$,
@f$ u @f$ and @f$ v @f$. These derivatives are calculated by finite difference schemes using a 9-point stencil (cf. fig below).

<p align="center">
  <img src="9-Point-Stencil.png" />
</p>

We use the notation: @f$ u(C2) @f$ = value of @f$ u @f$ in cell @f$ C2 @f$.
The derivatives in cell 5 are calculated as follows:

\f{eqnarray*}{
\frac{\partial u}{\partial x} &=& \frac{u(C5)-u(C4)}{\Delta x}\\
\frac{\partial u}{\partial y} &=& \frac{u(C5)-u(C8)}{\Delta y}\\
\frac{\partial^2 u}{\partial x^2} &=& \frac{u(C6)-2u(C5)+u(C4)}{(\Delta x)^{2}}\\
\frac{\partial^2 u}{\partial y^2} &=& \frac{u(C2)-2u(C5)+u(C8)}{(\Delta y)^{2}}\\
\frac{\partial^2 u}{\partial x \partial y} &=& \frac{u(C3)-u(C1)-u(C9)+u(C7)}{4\cdot \Delta x\cdot \Delta y}\\
\f}

Similarly, derivatives of @f$ h @f$ and @f$ v @f$ are calculated.

# Masonry Sand

## With Friction

<p align="center">
  <img src="dam-break-landslide-sand-theta_35.gif" />
</p>

## Without Friction

<p align="center">
  <img src="dam-break-landslide-sand-zero-friction-theta_35.gif" />
</p>

# Carborundum

<p align="center">
  <img src="dam-break-landslide-carborundum-theta_35.gif" />
</p>

*/
