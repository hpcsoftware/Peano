// This file is part of the ExaHyPE2 project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#pragma once

#include "Constants.h"
#include "VariableShortcuts.h"

#include "tarch/Assertions.h"
#include "tarch/NonCriticalAssertions.h"
#include "tarch/la/Vector.h"

namespace applications::exahype2::swe {
  #if defined(GPUOffloadingOMP)
  #pragma omp declare target
  #endif

  static inline GPUCallableMethod
  void eigenvalues(
    const double* const __restrict__             Q,
    const tarch::la::Vector<Dimensions, double>& x,
    const tarch::la::Vector<Dimensions, double>& h,
    const double                                 t,
    const double                                 dt,
    const int                                    normal,
    double* const __restrict__                   L
  ) InlineMethod;

  static inline GPUCallableMethod
  double maxEigenvalue(
    const double* const __restrict__             Q,
    const tarch::la::Vector<Dimensions, double>& x,
    const tarch::la::Vector<Dimensions, double>& h,
    const double                                 t,
    const double                                 dt,
    const int                                    normal
  ) InlineMethod;

  static inline GPUCallableMethod
  void flux(
    const double* const __restrict__             Q,
    const tarch::la::Vector<Dimensions, double>& x,
    const tarch::la::Vector<Dimensions, double>& h,
    const double                                 t,
    const double                                 dt,
    const int                                    normal,
    double* const __restrict__                   F
  ) InlineMethod;

  static inline GPUCallableMethod
  void nonconservativeProduct(
    const double* const __restrict__             Q,
    const double* const __restrict__             deltaQ,
    const tarch::la::Vector<Dimensions, double>& x,
    const tarch::la::Vector<Dimensions, double>& h,
    const double                                 t,
    const double                                 dt,
    const int                                    normal,
    double* const __restrict__                   BTimesDeltaQ
  ) InlineMethod;

  #if defined(GPUOffloadingOMP)
  #pragma omp end declare target
  #endif
} // namespace applications::exahype2::swe

#include "SWE.cpph"
